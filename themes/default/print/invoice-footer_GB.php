<table style="border-top:2px solid #555; text-align:center; width:-{-$order.page_width-}-;" width="-{-$order.page_width-}-" align="center">
	<tr>
		<td style="text-align:center; font-size:-{-$order.font-}-;" align="center">
			Payment should be made within -{-$prefs.paydays-}- days of Invoice date.<br>
			Payment can be made to<br>
			Creditcard via PayPal to email address : -{-$company.email-}-<br>
			IBAN BANK ACCOUNT -{-$company.bank-}- -- BIC/SWIFT CODE: -{-$company.swift-}-<br>
			Addres to : -{-$company.name-}- . <b style="font-size:-{-$order.font-}-;">Always mention your invoice number</b>
		</td>
	</tr>
</table>
<table style="border-top:1px solid #555; text-align:center; width:-{-$order.page_width-}-;" width="-{-$order.page_width-}-" align="center">
	<tr>
		<td style="text-align:center; font-size:-{-$order.font-}-;" align="center">
			-{-$company.name-}-<br>
			Bank account #-{-$company.bank-}- || VAT #-{-$company.vat-}- || COC #-{-$company.coc-}-
		</td>
	</tr>
</table>
<script>
	-{-if $order.print=="print"-}-
window.print();
	-{-/if-}-
</script>
</body>
</html>
