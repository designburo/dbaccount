<table style="border-top:2px solid #555; text-align:center; width:-{-$order.page_width-}-;" width="-{-$order.page_width-}-" align="center">
	<tr>
		<td style="text-align:center; font-size:-{-$order.font-}-;" align="center">
			Betaling dient te geschieden binnen -{-$prefs.paydays-}- dagen na factuurdatum.<br>
			Betalingen kunnen voldaan worden per<br>
			Creditcard via PayPal naar e-mailadres : -{-$company.email-}-<br>
			Bankoverschrijving: Rekening #-{-$company.bank-}- -- <b style="font-size:-{-$order.font-}-;">Vermeld altijd uw factuurnummer</b> --
		</td>
	</tr>
</table>
<table style="border-top:1px solid #555; text-align:center; width:-{-$order.page_width-}-;" width="-{-$order.page_width-}-" align="center">
	<tr>
		<td style="text-align:center; font-size:-{-$order.font-}-;" align="center">
			-{-$company.name-}-<br>
			Bankrekening #-{-$company.bank-}- || BTW #-{-$company.vat-}- || Kvk #-{-$company.coc-}-
		</td>
	</tr>
</table>
<script>
	-{-if $order.print=="print"-}-
window.print();
	-{-/if-}-
</script>
</body>
</html>
