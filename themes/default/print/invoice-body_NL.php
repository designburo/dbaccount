<table style="text-align:center; width:-{-$order.page_width-}-;" width="-{-$order.page_width-}-" align="center">
	<tr>
		<td style="text-align:left; vertical-align:top;" valign="top" align="left">
			<p style="font-size:-{-$order.font-}-;">
				<font color="#00f"><span style="color:#00f;">-{-$company.name-}-</span></font><br>
				-{-$company.address-}-<br>
				-{-$company.pcode-}- &nbsp; -{-$company.city-}- <br>
				-{-$company.country-}-<br>
				T. -{-$company.phone-}-<br>
				E. -{-$company.email-}-
			</p>
			<table>
				<tr>
					<td align="left" valign="top" style="text-align:left; background-color:#eee;vertical-align:top;"><u><span style="font-variant:underline;">Klant :</span></u></td>
					<td align="left" valign="top" style="text-align:left; background-color:#eee;vertical-align:top;"><u><span style="font-variant:underline;">-{-$customer.name-}-</span></u></td>
				</tr>
				-{-if $customer.contact -}-
				<tr>
					<td align="left" style="text-align:left;">t.a.v. :</td>
					<td align="left" style="text-align:left;">-{-$customer.contact-}-</td>
				</tr>
				-{-/if-}-
				<tr>
					<td align="left" valign="top" style="text-align:left;vertical-align:top;">Adres :</td>
					<td align="left" valign="top" style="text-align:left;vertical-align:top;">
						-{-$customer.address-}-<br>
						-{-$customer.pcode-}- &nbsp;-{-$customer.city-}-<br>
						-{-$customer.country-}-
					</td>
				</tr>
			</table>
		</td>
		<td style="text-align:right;" align="right" style="max-width:200px;">
			<table align="right" style="padding:5px; border:1px solid #555; text-align:right;">
				<tr>
					<td colspan="2" align="left" style="border-bottom:1px solid #555; text-align:left;">
						Faktuur details
					</td>
				</tr>
				<tr>
					<td align="left" style="text-align:left;">
						Factuur nr :
					</td>
					<td align="right" style="text-align:right;">
					-{-$order.number-}-
					</td>
				</tr>
				<tr>
					<td align="left" style="text-align:left;">
						Factuur datum :
					</td>
					<td align="right" style="text-align:right;">
					-{-$order.date-}-
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" style="text-align:center;">
						<img src="-{-$order.qrcode-}-" align="center" width="150" style="text-align:center; width:150px;">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<p></p>

<table cellpadding="5" cellspacing="0" border="0" style="text-align:center; width:-{-$order.page_width-}-;" width="-{-$order.page_width-}-" align="center">
<tr>
	<td width="10%" style="width:10%;text-align:left; border-bottom:1px solid #555;" align="left">
		-{-if $order.hasitems-}-Aantal-{-/if-}-
	</td>
	<td width="50%" style="width:50%;text-align:left; border-bottom:1px solid #555;" align="left">
		-{-if $order.hasitems-}-Item-{-/if-}-
	</td>
	<td width="20%" style="width:20%;text-align:right; border-bottom:1px solid #555;" align="right">
		-{-if $order.hasitems-}-Eenheid kosten-{-/if-}-
	</td>
	<td width="20%" style="width:20%;text-align:right; border-bottom:1px solid #555;" align="right">
		Prijs
	</td>
</tr>
-{-foreach from=$items item=value-}-
<tr>
	<td width="10%" style="width:10%;text-align:left; border-bottom:1px solid #999;" align="left">
		-{-if $order.hasitems-}--{-$value.amount-}--{-/if-}-
	</td>
	<td width="50%" style="width:50%;text-align:left; border-bottom:1px solid #999;" align="left">
		-{-if $order.hasitems-}--{-$value.item_name-}--{-/if-}-
	</td>
	<td width="20%" style="width:20%;text-align:right; border-bottom:1px solid #999;" align="right">
		-{-if $order.hasitems-}-&-{-$prefs.currency-}-; -{-$value.price-}--{-/if-}-
	</td>
	<td width="20%" style="width:20%;text-align:right; border-bottom:1px solid #999;" align="right">
		-{-if $order.hasitems-}-&-{-$prefs.currency-}-; -{-$value.price_total-}--{-/if-}-
	</td>
</tr>
-{-/foreach-}-
<tr><td></td></tr>
<tr>
	<td></td><td></td>
	<td width="20%" style="width:20%;text-align:right;" align="right">Bruto</td>
	<td width="20%" style="width:20%;text-align:right;" align="right">&-{-$prefs.currency-}-; -{-$order.amount_ex-}-</td>
</tr>
<tr>
	<td></td><td></td>
	<td width="20%" style="width:20%;text-align:right;" align="right">BTW</td>
	<td width="20%" style="width:20%;text-align:right;" align="right"><span style=" border-bottom:4px double #555;">&-{-$prefs.currency-}-; -{-$order.vat-}-</span></td>
</tr>
<tr><td></td></tr>
<tr>
	<td></td><td></td>
	<td width="20%" style="width:20%;text-align:right;" align="right">Factuur bedrag</td>
	<td width="20%" style="width:20%;text-align:right;color:blue;" align="right"><font color="#0000FF">&-{-$prefs.currency-}-; -{-$order.amount_in-}-</font></td>
</tr>
-{-if $order.status == "closed"-}-
<tr>
	<td></td><td></td>
	<td width="20%" style="width:20%;text-align:right;" align="right"></td>
	<td width="20%" style="width:20%;text-align:right;color:blue;" align="right"><span style="background-color:yellow;"> BETAALD </span></td>
</tr>
-{-/if-}-
<tr><td></td></tr>
<tr>
	<td colspan="4" style="border-bottom:1px solid #999; text-align:left;" align="left">
		Notitie
	</td>
</tr>
-{-if $order.description != false-}-
<tr>
	<td colspan="4" style="text-align:left;" align="left">
		-{-$order.description-}-
	</td>
</tr>
-{-/if-}-
</table>
