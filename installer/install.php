<?php session_start();
ERROR_REPORTING(0);
if(isset($_SESSION['force']) && $_SESSION['force']=="1")
{
	$force=true;
} else {
	$force=false;
}
if (getSettings()!==false && !isset($_GET['force']) && $force==false) {
	print_r($_SESSION);
	exit;
	header('Location: ../index.php');
} else {
	$_SESSION['force']="1";
}
if(isset($_GET['force']))
{
	$_SESSION['force']="1";
}
//print_r($_SESSION);
?>
<html>
<head>
	<title>DBaccount Installer</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<script
				src="https://code.jquery.com/jquery-3.1.1.min.js"
				integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
				crossorigin="anonymous">
	</script>
	<link rel="apple-touch-icon" sizes="180x180" href="../apple-touch-icon.png">
	<link rel="icon" type="image/png" href="../favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="../favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="../manifest.json">
	<link rel="mask-icon" href="../safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
	<link rel="stylesheet" href="../assets/css/uikit.almost-flat.css">
	<link rel="stylesheet" href="../assets/css/style-add.css">
	<script src="../assets/js/uikit.min.js"></script>
	<script src="../assets/js/components/notify.min.js"></script>
</head>
<body>
	<div class="uk-container uk-container-center">
		<nav class="uk-navbar">
			<a href="#" class="uk-navbar-brand uk-text-left"><img class="db-logo padme-small" src="../assets/images/logo.svg"></a>
		</nav>

		<p></p>
		<div class="uk-grid uk-grid-small uk-grid-divider">
      <div class="uk-width-large-1-1 uk-width-small-1-1">
      	<div class="uk-panel uk-panel-box uk-panel-box-secondary">

      <?php

			function setHtaccess() {
				//var_dump($_SESSION);
				$current = file_get_contents("../.htaccess");
				$search = '/dbaccount/';
				$replace = $_SESSION['path'];
				$new = str_replace($search,$replace,$current);
				file_put_contents("../.htaccess",$new);
			}
			function getSettings()
			{
				$ret= file_get_contents("../include/settings.json");
				return $ret;
			}
      // BEGIN CODE
      $step = (isset($_GET['step']) && $_GET['step'] != '') ? $_GET['step'] : '';
      switch($step){
        case '1':
        step_1();
        break;
        case '2':
        step_2();
        break;
				case '2a':
				step_2a();
				break;
        case '3':
        step_3();
        break;
        default:
        step_1();
      }
      ?>
      <body>
        <?php
        function step_1() {
					if (isset($_SESSION['n'])) {
					 	unset($_SESSION['n']);
					}
					if (isset($_SESSION['k'])) {
					 	unset($_SESSION['k']);
					}
         if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['agree'])){
          header('Location: install.php?step=2');
          exit;
         }
         if($_SERVER['REQUEST_METHOD'] == 'POST' && !isset($_POST['agree'])){
           echo '<h1 class="uk-panel-title">Installation step 1</h1>';
          echo '<p class="uk-text-danger uk-text-bold">You must agree to the license.</p>';
         }
        ?>
        <h1 class="uk-panel-title uk-text-primary">Installation step 1</h1>
         <h2>License</h2>
         <p>This software is provided as. You are granted the use of this sofware for yourself or your company.</p>
         <p>This software is copyrighted to Designburo.nl. Unauthorized copying of dbAccount, via any medium is strictly prohibited Proprietary and confidential.</p>
         <p>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
            INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
            FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
            IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
            ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
            CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
            WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</p>
          <p>If you encounter problems or bugs withing this software, please contact designburo.nl and we take issues under advisement.
         <form action="install.php?step=1" method="post">
         <p>
          I agree to the license
          <input type="checkbox" name="agree" />
         </p>
          <input type="submit" value="Continue" />
         </form>
        <?php
        }

        function step_2(){
					$pre_error="";
        if($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['pre_error'] ==''){
         header('Location: install.php?step=2a');
         exit;
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['pre_error'] != '')

         echo '<p class="uk-text-danger uk-text-bold">'.$_POST['pre_error'].'</p>';

        if (phpversion() < '5.3') {
         $pre_error = 'You need to have PHP5.3 or above installed for DBaccount to work propperly!<br />';
        }
        if (!extension_loaded('mysql')) {
         $pre_error .= 'DBaccount need the MySQL extension!<br />';
        }
        if (!is_writable('../include/config.php')) {
         $pre_error .= 'config.php needs to be writable!';
        }
        ?>
        <h1 class="uk-panel-title uk-text-primary">Installation step 2</h1>

        <h2>Requirements</h2>
        <table class="uk-table" width="100%">
          <thead>
            <tr>
              <th></th>
              <th>Installed</th>
              <th>Required</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
        <tr>
         <td>PHP Version</td>
         <td><?php echo phpversion(); ?></td>
         <td>5.3+</td>
         <td><?php echo (phpversion() >= '5.3') ? '<span class="uk-text-success">Ok</span>' : '<span class="uk-text-danger">Not Ok</span>'; ?></td>
        </tr>
        <tr>
         <td>MySQL:</td>
         <td><?php echo extension_loaded('mysql') ? 'On' : 'Off'; ?></td>
         <td>On</td>
         <td><?php echo extension_loaded('mysql') ? '<span class="uk-text-success">Ok</span>' : '<span class="uk-text-danger">Not Ok</span>'; ?></td>
        </tr>
        <tr>
         <td>GD:</td>
         <td><?php echo extension_loaded('gd') ? 'On' : 'Off'; ?></td>
         <td>On</td>
         <td><?php echo extension_loaded('gd') ? '<span class="uk-text-success">Ok</span>' : '<span class="uk-text-danger">Not Ok</span>'; ?></td>
        </tr>
        <tr>
         <td>config.php</td>
         <td><?php echo is_writable('../include/config.php') ? 'Writable' : 'Unwritable'; ?></td>
         <td>Writable</td>
         <td><?php echo is_writable('../include/config.php') ? '<span class="uk-text-success">Ok</span>' : '<span class="uk-text-danger">Not Ok</span>'; ?></td>
        </tr>
      </tbody>
        </table>
        <form action="install.php?step=2" method="post">
         <input type="hidden" name="pre_error" id="pre_error" value="<?php echo $pre_error;?>" />
         <input type="submit" name="continue" value="Continue" />
        </form>
      <?php
      }

			function step_2a()
			{
				if(isset($_POST['submit']) && $_POST['submit']=="Continue") {
					$name=(isset($_POST['license_name'])) ? $_POST['license_name'] : "";
					$key=(isset($_POST['license_key'])) ? $_POST['license_key'] : "";

					if(empty($name) || empty($key)) {
						echo '<p class="uk-text-danger uk-text-bold">A license name and key is required.</p>';
					} else {
						 include( "../include/license.php" );
						 $lkey = new license_key();
						 $lkey->keylen=20;
						 //$lkey->software="dbaccount";
						 $validate=$lkey->codeValidate($key,$name."dbaccount");
						 if ($validate=="YES") {
							 $_SESSION['n']=$name;
							 $_SESSION['k']=$key;
							 header('Location: install.php?step=3');
						 } else {
							 echo '<p class="uk-text-danger uk-text-bold">This is not a valid combination of License name and License key.</p>';
						 }
				}
			}
					?>
					<h1 class="uk-panel-title uk-text-primary">Installation step 2a</h1>
		      <form class="uk-form uk-form-horizontal" method="post" action="install.php?step=2a">
						<fieldset class="data-uk-margin">
							<legend>License</legend>
		        	<section class="uk-form-row">
								<label class="uk-form-label" for="license_name">License Name</label>
								<div class="uk-form-controls">
		         			<input class="uk-width-1-1" type="text" name="license_name" value=''>
								</div>
		       		</section>
		       		<section class="uk-form-row">
								<label class="uk-form-label" for="license_key">License Key</label>
								<div class="uk-form-controls">
		         			<input class="uk-width-1-1" type="text" name="license_key" maxlength="24" value="">
								</div>
							</section>

							<section class="uk-form-row uk-margin-top">
								<div class="uk-form-icon uk-width-1-1">
									<i class="uk-icon-arrow-circle-right db-white"></i>
									<input name="submit" type="submit" class="uk-button uk-button-primary uk-width-1-1 db-white" value="Continue">
								</div>
							</section>
					</fieldset>
		        </form>
					<?php


			}
      function step_3() {

        if (isset($_POST['submit']) && $_POST['submit']=="Install!") {
         $database_host=(isset($_POST['database_host'])) ? $_POST['database_host'] : "";
         $database_name= (isset($_POST['database_name'])) ? $_POST['database_name'] : "";
         $database_username=isset($_POST['database_username'])?$_POST['database_username']:"";
         $database_password=isset($_POST['database_password'])?$_POST['database_password']:"";
				 $database_prefix=isset($_POST['database_prefix'])?$_POST['database_prefix']:"";
         $admin_name=isset($_POST['admin_name'])?$_POST['admin_name']:"";
         $admin_password=isset($_POST['admin_password'])?$_POST['admin_password']:"";
				 $license_name = (isset($_SESSION['n'])) ? $_SESSION['n'] : "";
				 $license_key = (isset($_SESSION['k'])) ? $_SESSION['k'] : "";

				 if (empty($admin_name) || empty($admin_password) || empty($database_host) || empty($database_username) || empty($database_name)) {
         	echo '<p class="uk-text-danger uk-text-bold">All fields are required! Please re-enter.</p>';
				 } else {
					 if (empty($license_name) || empty($license_key)) {
						 echo '<p class="uk-text-danger uk-text-bold">You cannot skip installer steps.</p>';
					 } else {

					error_reporting(0);
					$connection = new MySQLi($database_host, $database_username, $database_password, $database_name);
					if($connection->connect_errno > 0) {
						echo '<p class="uk-text-danger uk-text-bold">Could not connect to database, please re-check your details [' . $connection->connect_error . ']</p>';
					} else {
						if (ob_get_level() == 0) ob_start();
						$connection->set_charset("utf8");
						$file = file_get_contents('../sql/dbaccounts.sql');
						$file = str_replace("dba_",$database_prefix,$file);
						preg_match_all('/CREATE(.*?);/s', $file, $matches);
						//echo "<pre>";
						//print_r($matches[0]);
						//echo "</pre>";
						//exit;

						foreach ($matches[0] as $query) {
							preg_match('/\`(.+?)\`/', $query, $name);
							echo "<p>Creating table ".$name[1];
							ob_flush();
							flush();
							$q=$connection->query($query);
							if (!$q) {
								echo $connection->error;
								exit;
							}
							echo ".. done</p>";
						}
						ob_end_flush();
						$connection->close();
						include( "../include/license.php" );
						$lkey = new license_key();
						$lkey->software="dbaccount";
						$lkey->reg_name=$license_name;
						$lkey->reg_key=$license_key;
						$lkey->db_host=$database_host;
						$lkey->db_name=$database_name;
						$lkey->db_user=$database_username;
						$lkey->db_pass=$database_password;
						$lkey->db_prefix=$database_prefix;
						$result=$lkey->activate();
						if(!$result['status']) {
							echo '<p class="uk-text-danger uk-text-bold">'.$result['info'].'</p>';
							exit;
						} else {
							echo '<p class="uk-text-success uk-text-bold">'.$result['info'].'</p>';
						}
						//exit;
				 /*
         if ($sql = file($file)) {
         $query = '';
         foreach($sql as $line) {
          $tsl = trim($line);
         if (($sql != '') && (substr($tsl, 0, 2) != "--") && (substr($tsl, 0, 1) != '#')) {
         $query .= $line;

         if (preg_match('/;\s*$/', $line)) {

          mysql_query($query, $connection);
          $err = mysql_error();
          if (!empty($err))
            break;
         $query = '';
				 */


				 $f=fopen("../include/config.php","w");
				 $database_inf="<?php\n";
         $database_inf .= "define('DATABASE_HOST', '".$database_host."');\n";
         $database_inf .= "define('DATABASE_NAME', '".$database_name."');\n";
				 $database_inf .= "define('DATABASE_PREFIX', '".$database_prefix."');\n";
         $database_inf .= "define('DATABASE_USERNAME', '".$database_username."');\n";
         $database_inf .= "define('DATABASE_PASSWORD', '".$database_password."');\n";
				 $database_inf .= "define('ADMIN_NAME', '".$admin_name."');\n";
         $database_inf .= "define('ADMIN_PASSWORD', '".$admin_password."');\n";
				 $database_inf .= "define('LICENSE_NAME', '".$license_name."');\n";
				 $database_inf .= "define('LICENSE_KEY', '".$license_key."');\n";
				 $pad="http://".$_SERVER['SERVER_NAME'].dirname($_SERVER['REQUEST_URI']);
				 $pad=substr($pad,0, -9);
				 $database_inf .= "define('DBA_URL', '".$pad."');\n";
         $database_inf .= "?>\n";
        if (fwrite($f,$database_inf)>0) {
         fclose($f);
        }
				setHtaccess();
				?>
        	<form class="uk-form uk-form-horizontal" method="post" action="../index.php">
						<section class="uk-form-row uk-margin-top">
							<div class="uk-form-icon uk-width-1-1">
								<i class="uk-icon-arrow-circle-right db-white"></i>
								<input name="submit" type="submit" class="uk-button uk-button-primary uk-width-1-1 db-white" value="Installed.. Click to login with your username and password. Be sure to rename you installation folder.">
							</div>
						</section>
					</form>
					<?php
					exit;
        }
			}
		}
			} else {
				$license_name = (isset($_SESSION['n'])) ? $_SESSION['n'] : "";
				$license_key = (isset($_SESSION['k'])) ? $_SESSION['k'] : "";
				if (empty($license_name) || empty($license_key)) {
					echo '<p class="uk-text-danger uk-text-bold">You cannot skip installer steps.</p>';
					exit;
				} else {
					echo '<p class="uk-text-success uk-text-bold">Valid license key detected.</p>';
					if (file_exists("../include/config.php")) {
						include "../include/config.php";
						if(defined("DATABASE_HOST")) {
							$database_host= DATABASE_HOST;
							$database_name= DATABASE_NAME;
							$database_username=DATABASE_USERNAME;
							$database_password=DATABASE_PASSWORD;
							$database_prefix=DATABASE_PREFIX;
							$admin_name=ADMIN_NAME;
							$admin_password=ADMIN_PASSWORD;
						} else {
					$database_host= "";
					$database_name= "";
					$database_username="";
					$database_password="";
					$database_prefix="";
					$admin_name="";
					$admin_password="";
					}
				}
			}

      ?>
			<h1 class="uk-panel-title uk-text-primary">Installation step 3</h1>
      <form class="uk-form uk-form-horizontal" method="post" action="install.php?step=3">
				<fieldset class="data-uk-margin">
					<legend>Database and admin information</legend>
        	<section class="uk-form-row">
						<label class="uk-form-label" for="database_host">Database Host</label>
						<div class="uk-form-controls">
         			<input type="text" name="database_host" value='<?php echo ($database_host != "") ? $database_host : "localhost"; ?>' size="30">
						</div>
       		</section>
       		<section class="uk-form-row">
						<label class="uk-form-label" for="database_name">Database Name</label>
						<div class="uk-form-controls">
         			<input type="text" name="database_name" size="30" value="<?php echo $database_name; ?>">
						</div>
					</section>
					<section class="uk-form-row">
						<label class="uk-form-label" for="database_prefix">Database Prefix</label>
						<div class="uk-form-controls">
         			<input type="text" name="database_prefix" size="30" value="<?php echo ($database_prefix != "") ? $database_prefix : "dba_"; ?>">
						</div>
					</section>
       		<section class="uk-form-row">
				 		<label class="uk-form-label" for="database_username">Database Username</label>
				 		<div class="uk-form-controls">
         			<input type="text" name="database_username" size="30" value="<?php echo $database_username; ?>">
				 		</div>
       		</section>
       		<section class="uk-form-row">
				 		<label class="uk-form-label" for="database_password">Database Password</label>
				 		<div class="uk-form-controls">
         			<input type="text" name="database_password" size="30" value="<?php echo $database_password; ?>">
				 		</div>

	        </section>
	        <section class="uk-form-row uk-margin-large-top">
						<label class="uk-form-label" for="admin_name">Admin login</label>
						<div class="uk-form-controls">
	         		<input type="text" name="admin_name" size="30" value="<?php echo $admin_name; ?>">
					 	</div>
	       	</section>
	       	<section class="uk-form-row">
					 <label class="uk-form-label" for="admin_password">Admin password</label>
					 <div class="uk-form-controls">
	         	<input name="admin_password" type="text" size="30" maxlength="15" value="<?php echo $admin_password; ?>">
				 	 </div>
	        </section>
					<section class="uk-form-row uk-margin-top">
						<div class="uk-form-icon uk-width-1-1">
							<i class="uk-icon-arrow-circle-right db-white"></i>
							<input name="submit" type="submit" class="uk-button uk-button-primary uk-width-1-1 db-white" value="Install!">
						</div>
					</section>
			</fieldset>
        </form>
      <?php
      }
		}


      // END CODE
      ?>

    </div>
  </div>
		</div>
		<div class="footer uk-grid uk-grid-small uk-grid-divider">
		<div class="uk-width-large-1-1 uk-width-small-1-1">
			<div class="uk-panel uk-width-1-1 uk-margin-top">
				<p class="uk-text-right uk-text-small">DB account &copy;2017 - <span><img style="height:16px;" src="../assets/images/dsgnbro.png"></span></p>
			</div>
	</div>
	</div>

	<!--<script src="https://getuikit.com/migrate.min.js"></script>-->
</body>
</html>
