<?php

define ("_QUOTES_LABEL_NEW", "New");
define ("_QUOTES_LABEL_EDIT","Edit");
define ("_QUOTES_ACTION_NEW_TITLE", "Add new quote");
define ("_QUOTES_INFO_HEADING","Quotes");
define ("_QUOTES_INFO_TEXT","Manage quotes");
define ("_QUOTES_INFO_NORESULTS","You have no quotes defined yet.<BR>Use <i class=\"uk-icon-plus\"></i> in the panel below to create a quote.");
define ("_QUOTES_INFO_NEW","Use the panel below to create a new quote.");
define ("_QUOTES_INFO_EDIT","Use the panel below to edit.");
define ("_QUOTES_NO_CUSTOMERS", "You have not created a customer yet. Please do this first by clicking ");
define ("_QUOTES_NO_CUSTOMERS_LINK", "here");
define ("_QUOTES_CUSTOMERS_NAME", "Customer");
define ("_QUOTES_CONTACTS_NAME", "Contact");
define ("_QUOTES_SELECT_CONTACT", "Select a contact");
define ("_QUOTES_SELECT_CUSTOMER", "Select a customer");
define ("_QUOTES_DESCRIPTION", "Description");
define ("_QUOTES_DESCRIPTION_HELP", "This is additional text you can put on your invoice besides the items.");
define ("_QUOTES_NOTE_INTERNAL", "Internal note");
define ("_QUOTES_NOTE_INTERNAL_HELP", "This is a note for internal purposes.");
define ("_QUOTES_VAT", "VAT");
define ("_QUOTES_DATE", "Quote Date");
define ("_QUOTES_NO_ITEMS", "You have not created any items yet");
define ("_QUOTES_ITEM_VAT_PERCENTAGE", "VAT %");
define ("_QUOTES_ITEM_AMOUNT", "Amount");
define ("_QUOTES_ITEM_ITEM", "Item");
define ("_QUOTES_ITEM_ITEMS", "Items");
define ("_QUOTES_ITEM_PRICE", "Price per item");
define ("_QUOTES_ITEM_TOTAL", "Total price");
define ("_QUOTES_ITEM_ADD_TEMPLATE", "Add from template");
define ("_QUOTES_ITEM_ADD_ROW", "Add Row");
define ("_QUOTES_ITEM_DEL_ROW", "Delete Row");
define ("_QUOTES_TOTAL_EX", "Total excl. VAT");
define ("_QUOTES_TOTAL_IN", "Total incl. VAT");
define ("_QUOTES_TOTAL_VAT", "Total VAT");
define ("_QUOTES_PRINT", "DB Account opened a TAB or Window for viewing or printing the quote.");


//view_mode_post_types

define ("_QUOTES_VIEW_HEADER", "details");
define ("_QUOTES_INFO_VIEW","Viewing");
define ("_QUOTES_VIEW_DATE","Date");
define ("_QUOTES_VIEW_CUSTOMER","Customer");
define ("_QUOTES_VIEW_CUSTOMER_ADDRESS","Address");
define ("_QUOTES_VIEW_ORDER_NR","Quote #");
define ("_QUOTES_VIEW_TOTALS","Totals");
define ("_QUOTES_VIEW_INCOME","Income");
define ("_QUOTES_VIEW_EXPENSES","Expenses");
define ("_QUOTES_VIEW_STATUS","Status");

define ("_QUOTES_VIEW_EXPENSE_DATE","Date");
define ("_QUOTES_VIEW_EXPENSE_NAME","Name");
define ("_QUOTES_VIEW_EXPENSE_TOTAL_EX","Total excl. VAT");
define ("_QUOTES_VIEW_EXPENSE_TOTAL_VAT","Total VAT");
define ("_QUOTES_VIEW_EXPENSE_TOTAL_IN","Total incl. VAT");

// List quotes
define ("_QUOTES_LIST_ORDER_NR", "Quote #");
define ("_QUOTES_LIST_ORDER_TOTAL", "Total");
define ("_QUOTES_LIST_ORDER_OPEN", "Open");
define ("_QUOTES_LIST_ORDER_DATE", "Date");
define ("_QUOTES_LIST_ORDER_AGE", "Age");
define ("_QUOTES_LIST_ORDER_STATUS", "Status");
define ("_QUOTES_LIST_ORDER_ACTIONS", "Quote actions");
define ("_QUOTES_LIST_ACTIONS_PAYMENT", "Payments");
define ("_QUOTES_LIST_ACTIONS_EDIT", "Edit quote");
define ("_QUOTES_LIST_ACTIONS_VIEW", "View invoice");
define ("_QUOTES_LIST_ACTIONS_PRINT", "Print invoice");
define ("_QUOTES_LIST_ACTIONS_REMINDER", "Email reminder");
define ("_QUOTES_LIST_ACTIONS_PDF", "Create PDF");
define ("_QUOTES_LIST_ACTIONS_DELETE", "Delete expense");
define ("_QUOTES_LIST_ACTIONS_DELETE_CONFIRM", "Are you sure that you want to delete this quote ? All details will be deleted.");

?>
