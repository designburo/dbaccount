<?php

define ("_CUSTOMER_INFO_NORESULTS","You have no Customers yet.<BR>Use <i class=\"uk-icon-plus\"></i> in the panel on the right to create a customer.");
define ("_CUSTOMER_ACTION_NEW", "Add new customer");
define ("_CUSTOMER_ACTION_EDIT", "Edit customer");
define ("_CUSTOMER_INFO_HEADING","Customers");
define ("_CUSTOMER_CUSTOMER","Customer");
define ("_CUSTOMER_TAB_CONTACTS","Contacts");
define ("_CUSTOMER_TAB_ORDERS","Orders");
define ("_CUSTOMER_TAB_QUOTES","Quotes");
define ("_CUSTOMER_TAB_NOTES","Notes");
define ("_CUSTOMER_TYPE","Type");
define ("_CUSTOMER_TYPE_CUSTOMER","Customer");
define ("_CUSTOMER_TYPE_PAYABLE","Payable");
define ("_CUSTOMER_INFO_TEXT","Manage your customers");
define ("_CUSTOMER_INFO_NEW","Use the form in the panel on the right to create a new customer.");

?>
