<?php

// Menu
define ("_HOME","Dashboard");
define ("_INVOICES","Invoices");
define ("_ORDERS","Orders");
define ("_ESTIMATES","Quotes");
define ("_EXPENSES","Expenses");
define ("_CUSTOMERS","Customers");
define ("_CONTACTS","Contacts");
define ("_PAYMENTS","Payments");
define ("_LOGOUT","Logout");
define ("_TRANSLATE","Translate");
define ("_ACTION_NEW_ORDER","New order");
define ("_ACTION_NEW_QUOTE","New quote");
define ("_ACTION_NEW_CONTACT","New contact");
define ("_ACTION_NEW_EXPENSE","New expense");
define ("_ACTION_SEARCH","Type to search");

define ("_LOGIN_USERNAME","Username");
define ("_LOGIN_PASSWORD","Password");
define ("_LOGIN_LOGIN","Login");
define ("_LOGIN_SHOW_PASSWORD","Show password");
define ("_LOGIN_HIDE_PASSWORD","Hide password");

define ("_GENERAL_NAME", "Name");
define ("_GENERAL_USER", "User");
define ("_GENERAL_FNAME", "First name");
define ("_GENERAL_LNAME", "Surname");
define ("_GENERAL_INITIALS", "Initials");
define ("_GENERAL_TITLE", "Title");
define ("_GENERAL_ADDR1", "Address 1");
define ("_GENERAL_ADDR2", "Address 2");
define ("_GENERAL_ADDRESS", "Address");
define ("_GENERAL_ZIP", "Zipcode");
define ("_GENERAL_CITY", "City");
define ("_GENERAL_COUNTRY", "Country");
define ("_GENERAL_EMAIL", "Email address");
define ("_GENERAL_WEBSITE", "Website");
define ("_GENERAL_VAT", "VAT Number");
define ("_GENERAL_COC", "Chamber of Commerce");
define ("_GENERAL_PHONE", "Phone");
define ("_GENERAL_CONTACT", "Contact person");
define ("_GENERAL_SKYPE", "Skype account");
define ("_GENERAL_BANK", "Bank Account");
define ("_GENERAL_NOTE", "Note");
define ("_GENERAL_LOGO", "Company logo");
define ("_GENERAL_DELETE", "Delete");
define ("_GENERAL_EDIT", "Edit");
define ("_GENERAL_ADDED", "Added");
define ("_GENERAL_LAST_MODIFIED", "Last modified");
define ("_GENERAL_CURRENCY", "Currency");
define ("_EMAIL_SUBJECT", "Subject");
define ("_EMAIL_TO", "To");
define ("_EMAIL_BODY", "Message");
define ("_EMAIL_HEADER", "Email");
define ("_POST_EMAIL_SEND_OK", "Mail send");
define ("_NOTE_DELETE_OK", "Note succesfully deleted");



define ("_INSTALL","Install");
define ("_UNKNOWN","Unknown");
define ("_DB_ERROR", "Database error");
define ("_DBA_ERROR", "Database error");
define ("_DB_NA", "No Database available. ");
define ("_DB_INSTALL", "Install Database?");
define ("_DB_NOSQL", "No SQL file in sql folder.");
define ("_DB_SETTINGS","Please adjust database setting in include/dba_class.php lines 8-12 first.");
define ("_MSG_STATUS", "STATUS");

define ("_LICENSE_HEADER", "License information");
define ("_LICENSE_TO", "Licensed to");
define ("_LICENSE_KEY", "License key");
define ("_LICENSE_STATUS", "Status");
define ("_LICENSE_SHOW_KEY", "Show");
define ("_LICENSE_HIDE_KEY", "Hide");
define ("_LICENSE_INSTALLED", "License installed on");
define ("_LICENSE_VALID", "License valid till");
define ("_LICENSE_DAYS_LEFT", "days left");
define ("_LICENSE_DAY_LEFT", "day left");

?>
