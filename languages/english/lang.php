<?php

include ("lang-general.php");
include ("lang-vat.php");
include ("lang-customer.php");
include ("lang-contact.php");
include ("lang-item.php");
include ("lang-prefs.php");
include ("lang-order.php");
include ("lang-company.php");
include ("lang-post.php");
include ("lang-expenses.php");
include ("lang-payments.php");
include ("lang-taxes.php");
include ("lang-quote.php");


define ("_MONEY_NOT_PAYED", "Not payed");
define ("_MONEY_DONE_BUSINESS", "Previous orders");
define ("_MONEY_OPEN_QOUTES", "Open quotes");
define ("_MONEY_POTENTIAL_AMOUNT", "Potential business");
define ("_MONEY_INCL_TAX", "incl. tax");
define ("_MONEY_EXCL_TAX", "excl. tax");
define ("_MONEY_QUOTES", "Quotes");
define ("_MONEY_ORDERS", "Orders");


?>
