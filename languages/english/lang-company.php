<?php

define ("_COMPANY_INFO_HEADING","Changing company details");
define ("_COMPANY_INFO_TEXT","Use the panel on the right to change company details.");
define ("_COMPANY_INFO_NORESULTS","You have not created your bussiness details yet<BR>Use the panel on the right and fill in the form.");
define ("_COMPANY_LEGEND", "Company");
define ("_COMPANY_PANEL_GENERAL", "General");
define ("_COMPANY_PANEL_CONTACT", "Contact");
define ("_COMPANY_PANEL_COMMERCE", "Commerce");

?>
