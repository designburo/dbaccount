<?php

define ("_ORDERS_LABEL_NEW", "New");
define ("_ORDERS_LABEL_EDIT","Edit");
define ("_ORDERS_ACTION_NEW_TITLE", "Add new order");
define ("_ORDERS_INFO_HEADING","Orders");
define ("_ORDERS_INFO_TEXT","Manage orders");
define ("_ORDERS_INFO_NORESULTS","You have no orders defined yet.<BR>Use <i class=\"uk-icon-plus\"></i> in the panel below to create an order.");
define ("_ORDERS_INFO_NEW","Use the panel below to create a new order.");
define ("_ORDERS_INFO_EDIT","Use the panel below to edit.");
define ("_ORDERS_NO_CUSTOMERS", "You have not created a customer yet. Please do this first by clicking ");
define ("_ORDERS_NO_CUSTOMERS_LINK", "here");
define ("_ORDERS_CUSTOMERS_NAME", "Customer");
define ("_ORDERS_CONTACTS_NAME", "Contact");
define ("_ORDERS_SELECT_CONTACT", "Select a contact");
define ("_ORDERS_SELECT_CUSTOMER", "Select a customer");
define ("_ORDERS_DESCRIPTION", "Description");
define ("_ORDERS_DESCRIPTION_HELP", "This is additional text you can put on your invoice besides the items.");
define ("_ORDERS_NOTE_INTERNAL", "Internal note");
define ("_ORDERS_NOTE_INTERNAL_HELP", "This is a note for internal purposes.");
define ("_ORDERS_VAT", "VAT");
define ("_ORDERS_DATE", "Order Date");
define ("_ORDERS_NO_ITEMS", "You have not created any items yet");
define ("_ORDERS_ITEM_VAT_PERCENTAGE", "VAT %");
define ("_ORDERS_ITEM_AMOUNT", "Amount");
define ("_ORDERS_ITEM_ITEM", "Item");
define ("_ORDERS_ITEM_ITEMS", "Items");
define ("_ORDERS_ITEM_PRICE", "Price per item");
define ("_ORDERS_ITEM_TOTAL", "Total price");
define ("_ORDERS_ITEM_ADD_TEMPLATE", "Add from template");
define ("_ORDERS_ITEM_ADD_ROW", "Add Row");
define ("_ORDERS_ITEM_DEL_ROW", "Delete Row");
define ("_ORDERS_TOTAL_EX", "Total excl. VAT");
define ("_ORDERS_TOTAL_IN", "Total incl. VAT");
define ("_ORDERS_TOTAL_VAT", "Total VAT");
define ("_ORDERS_PRINT", "DB Account opened a TAB or Window for viewing or printing the order.");


//view_mode_post_types

define ("_ORDERS_VIEW_HEADER", "details");
define ("_ORDERS_INFO_VIEW","Viewing");
define ("_ORDERS_VIEW_DATE","Date");
define ("_ORDERS_VIEW_CUSTOMER","Customer");
define ("_ORDERS_VIEW_CUSTOMER_ADDRESS","Address");
define ("_ORDERS_VIEW_ORDER_NR","Order #");
define ("_ORDERS_VIEW_TOTALS","Totals");
define ("_ORDERS_VIEW_INCOME","Income");
define ("_ORDERS_VIEW_EXPENSES","Expenses");
define ("_ORDERS_VIEW_STATUS","Status");

define ("_ORDERS_VIEW_EXPENSE_DATE","Date");
define ("_ORDERS_VIEW_EXPENSE_NAME","Name");
define ("_ORDERS_VIEW_EXPENSE_TOTAL_EX","Total excl. VAT");
define ("_ORDERS_VIEW_EXPENSE_TOTAL_VAT","Total VAT");
define ("_ORDERS_VIEW_EXPENSE_TOTAL_IN","Total incl. VAT");

// List orders
define ("_ORDERS_LIST_ORDER_NR", "Order #");
define ("_ORDERS_LIST_ORDER_TOTAL", "Total");
define ("_ORDERS_LIST_ORDER_OPEN", "Open");
define ("_ORDERS_LIST_ORDER_DATE", "Date");
define ("_ORDERS_LIST_ORDER_AGE", "Age");
define ("_ORDERS_LIST_ORDER_STATUS", "Status");
define ("_ORDERS_LIST_ORDER_ACTIONS", "Order actions");
define ("_ORDERS_LIST_ACTIONS_PAYMENT", "Payments");
define ("_ORDERS_LIST_ACTIONS_EDIT", "Edit order");
define ("_ORDERS_LIST_ACTIONS_VIEW", "View invoice");
define ("_ORDERS_LIST_ACTIONS_PRINT", "Print invoice");
define ("_ORDERS_LIST_ACTIONS_REMINDER", "Email reminder");
define ("_ORDERS_LIST_ACTIONS_PDF", "Create PDF");
define ("_ORDERS_LIST_ACTIONS_DELETE", "Delete expense");
define ("_ORDERS_LIST_ACTIONS_DELETE_CONFIRM", "Are you sure that you want to delete this order ? All details will be deleted.");

?>
