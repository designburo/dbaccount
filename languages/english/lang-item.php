<?php

define ("_ITEMS", "Items");
define ("_ITEMS_LABEL_NEW", "New");
define ("_ITEMS_LABEL_EDIT","Edit");
define ("_ITEMS_ACTION_NEW_TITLE", "Add new item");
define ("_ITEMS_INFO_HEADING","Items");
define ("_ITEMS_INFO_TEXT","Manage items");
define ("_ITEMS_INFO_NORESULTS","You have no items defined yet.<BR>Use <i class=\"uk-icon-plus\"></i> in the panel below to create an item.");
define ("_ITEMS_INFO_NEW","Use the panel below to create a new item.");
define ("_ITEMS_INFO_EDIT","Use the panel below to edit.");
define ("_ITEMS_NAME", "Item name");
define ("_ITEMS_PRICE", "Item price");
define ("_ITEMS_VAT", "VAT");
define ("_ITEMS_LIST_NAME", "Name");
define ("_ITEMS_LIST_PRICE", "Price");
define ("_ITEMS_LIST_VAT", "VAT");
define ("_ITEMS_LIST_DESCRIPTION", "Description");
define ("_ITEMS_NO_VAT", "You have not created a VAT yet. Please do this first by clicking ");
define ("_ITEMS_NO_VAT_LINK", "here");
define ("_ITEMS_DESCRIPTION", "Item description (140 characters)");

?>
