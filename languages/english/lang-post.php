<?php

define ("_POST_PREFS_EMPTYNAME","Empty accounting name");
define ("_POST_PREFS_POST_OK","Settings saved");
define ("_POST_VAT_EMPTYNAME","Empty VAT name");
define ("_POST_VAT_ERRORVALUE", "VAT Value must not be empty and must be numeric.");
define ("_POST_VAT_NEW_POST_OK","New VAT created");
define ("_POST_VAT_EDIT_POST_OK","VAT succesfully edited");
define ("_POST_VAT_DELETE","VAT deleted");
define ("_POST_ITEMS_EMPTY_NAME","Empty item name");
define ("_POST_ITEMS_EMPTY_PRICE","Empty price");
define ("_POST_ITEMS_EMPTY_VAT","No VAT choosen");
define ("_POST_ITEMS_NEW_POST_OK","New item created");
define ("_POST_ITEMS_EDIT_POST_OK","Item succesfully edited");
define ("_POST_ITEMS_DELETE","Item deleted");

define ("_POST_ORDERS_EDIT_POST_OK","Order succesfully edited");
define ("_POST_ORDERS_NEW_POST_OK","New Order succesfully added");
define ("_POST_ORDERS_DELETE","Order deleted");

define ("_POST_QUOTES_EDIT_POST_OK","Order succesfully edited");
define ("_POST_QUOTES_NEW_POST_OK","New Order succesfully added");
define ("_POST_QUOTES_DELETE","Order deleted");

define ("_POST_COMPANY_NEW_POST_OK","New Company created");
define ("_POST_COMPANY_EDIT_POST_OK","Company succesfully edited");
define ("_POST_COMPANY_LOGO_TOBIG", "Logo exceeds 2 Mb");
define ("_POST_COMPANY_LOGO_WRONG", "Logo is not a jpeg, gif or png");

define ("_POST_CUSTOMER_NEW_POST_OK","New customer created");
define ("_POST_CUSTOMER_EDIT_POST_OK","Customer succesfully edited");
define ("_POST_ERRORS","Please fix the mentioned errors first.");
define ("_POST_EMAIL_ERROR_RETURN", "No return link found");
define ("_POST_EMAIL_ERROR_TO", "Empty recipient");
define ("_POST_EMAIL_ERROR_SUBJECT", "Empty subject");
define ("_POST_EMAIL_ERROR_BODY", "Empty mail content");
define ("_POST_SAVE","Save");
define ("_POST_EMPTY","Empty");

define ("_POST_CONTACT_NEW_POST_OK","New customer created");
define ("_POST_CONTACT_EDIT_POST_OK","Customer succesfully edited");
define ("_POST_CONTACT_PHOTO_TOBIG", "Photo exceeds 2 Mb");
define ("_POST_CONTACT_PHOTO_WRONG", "Photo is not a jpeg, gif or png");

define ("_POST_EXPENSES_NEW_POST_OK","New expense created");
define ("_POST_EXPENSES_EDIT_POST_OK","Expense succesfully edited");
define ("_POST_EXPENSES_FILE_TOBIG", "File exceeds 5 Mb");
define ("_POST_EXPENSES_FILE_WRONG", "File is not a jpeg, pdf or png");
define ("_POST_EXPENSES_FILE_NONAME", "A name for the file is empty");
define ("_POST_EXPENSES_DELETED", "Expense successfully deleted");

define ("_POST_PAYMENTS_NEW_POST_OK","New payment created");
define ("_POST_PAYMENTS_EDIT_POST_OK","Payment succesfully edited");
define ("_POST_PAYMENTS_DELETED", "Payment successfully deleted");

?>
