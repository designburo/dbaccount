<?php

define ("_PREFERENCES","Preferences");
define ("_PREFS_SETTINGS","Program settings");
define ("_PREFS_COMPANY","Your business");
define ("_PREFS_STATUS","Details");
define ("_PREFS_LANGUAGE","Language");
define ("_PREFS_THEME","Theme");
define ("_PREFS_APPNAME","Accounting name");
define ("_PREFS_TIMEZONE","Timezone");
define ("_PREFS_INFO_HEADING","Changing preferences");
define ("_PREFS_INFO_TEXT","Use the panel on the right to change the default preferences.");
define ("_PREFS_PREFERED_CURRENCY", "Prefered currency");
define ("_PREFS_SMTP_LEGEND", "SMTP Mail setup");
define ("_PREFS_SMTP_HOST", "Host");
define ("_PREFS_SMTP_PORT", "Port");
define ("_PREFS_SMTP_AUTH", "Authentication");
define ("_PREFS_SMTP_USER", "Username");
define ("_PREFS_SMTP_PASS", "Password");
define ("_PREFS_PAYDAYS", "Number of days for payments");
define ("_PREFS_ORDERNR", "Way to present ordernumber");
define ("_PREFS_COLORSCHEME", "Color variation");
?>
