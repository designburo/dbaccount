<?php

define ("_TAXES", "Taxes");
define ("_TAXES_LABEL_NEW", "New");
define ("_TAXES_LABEL_EDIT","Edit");
define ("_TAXES_ACTION_NEW_TITLE", "Add new item");
define ("_TAXES_INFO_HEADING","Taxes");
define ("_TAXES_INFO_TEXT","Tax overview");
define ("_TAXES_INFO_NORESULTS","Database has no orders or expenses. Come back here when you have add some.");
define ("_TAXES_INFO_NEW","Use the panel below to create a new item.");
define ("_TAXES_INFO_EDIT","Use the panel below to edit.");
define ("_TAXES_1_QUARTER", "1st quarter");
define ("_TAXES_2_QUARTER", "2nd quarter");
define ("_TAXES_3_QUARTER", "3rd quarter");
define ("_TAXES_4_QUARTER", "4th quarter");
define ("_TAXES_YEAR", "Year");
define ("_TAXES_IN_VAT", "Including VAT");
define ("_TAXES_EX_VAT", "Excluding VAT");
define ("_TAXES_VAT", "VAT");
define ("_TAXES_INCOMING", "Incoming");
define ("_TAXES_OUTGOING", "Outgoing");
define ("_TAXES_NOTHING", "No incoming or outgoing this quarter");
define ("_TAXES_TOTALS", "Totals");

?>
