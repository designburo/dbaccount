<?php

define ("_CONTACT_INFO_NORESULTS","You have no Contacts yet.<BR>Use <i class=\"uk-icon-plus\"></i> in the panel on the right to create a contact.");
define ("_CONTACT_ACTION_NEW", "Add new contact");
define ("_CONTACT_ACTION_EDIT", "Edit contact");
define ("_CONTACT_INFO_HEADING","Contacts");
define ("_CONTACT_CONTACT","Contact");
define ("_CONTACT_COMPANY","Company");
define ("_CONTACT_PHOTO","Contact photo");
define ("_CONTACT_TAB_CONTACTS","Contacts");
define ("_CONTACT_TAB_ORDERS","Orders");
define ("_CONTACT_TAB_QUOTES","Quotes");
define ("_CONTACT_TAB_NOTES","Notes");
define ("_CONTACT_INFO_TEXT","Manage your contacts");
define ("_CONTACT_INFO_NEW","Use the form in the panel on the right to create a new contact.");
define ("_CONTACT_UNKOWN","Unkown contact");
define ("_CONTACT_CUSTOMER_UNKOWN","A contact without a customer should not happen. Please contact Designburo.nl");
define ("_CONTACT_VIEW_DETAIL","Contact details");

?>
