<?php

define ("_VAT","VAT");
define ("_VAT_LABEL_NEW","New");
define ("_VAT_LABEL_EDIT","Edit");
define ("_VAT_ACTION_NEW", "Add new VAT");
define ("_VAT_INFO_HEADING","VAT's");
define ("_VAT_INFO_TEXT","Manage different VAT's");
define ("_VAT_INFO_NORESULTS","You have no VAT's defined yet.<BR>Use <i class=\"uk-icon-plus\"></i> in the panel on the right to create a VAT.");
define ("_VAT_INFO_NEW","Use the panel on the right to create a new VAT.");
define ("_VAT_INFO_EDIT","Use the panel on the right to edit.");
define ("_VAT_NAME", "VAT name");
define ("_VAT_VALUE", "VAT percentage");

?>
