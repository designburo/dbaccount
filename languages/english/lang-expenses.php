<?php

define ("_EXPENSES_LABEL_NEW", "New");
define ("_EXPENSES_LABEL_EDIT","Edit");
define ("_EXPENSES_ACTION_NEW_TITLE", "Add new expense");
define ("_EXPENSES_INFO_HEADING","Expenses");
define ("_EXPENSES_INFO_TEXT","Manage expenses");
define ("_EXPENSES_INFO_NORESULTS","You have no expenses defined yet.<BR>Use <i class=\"uk-icon-plus\"></i> in the panel below to create an expense.");
define ("_EXPENSES_INFO_NORESULTS_ON_COMPANY_PAGE","You have no expenses for this customer yet.");
define ("_EXPENSES_INFO_NEW","Use the panel below to create a new expense.");
define ("_EXPENSES_INFO_EDIT","Use the panel below to edit.");
define ("_EXPENSES_INFO_VIEW","Viewing");
define ("_EXPENSES_DESCRIPTION", "Description");
define ("_EXPENSES_DESCRIPTION_HELP", "This is additional text to describe your expense.");
define ("_EXPENSES_VAT", "VAT");
define ("_EXPENSES_DATE", "Expense date");
define ("_EXPENSES_CUSTOMER", "Expense company");
define ("_EXPENSES_CUSTOMER_NONE", "You have no Expense companies defined");
define ("_EXPENSES_NAME", "Expense name");
define ("_EXPENSES_TYPE", "Payment type");
define ("_EXPENSES_INVOICE", "Invoice number");
define ("_EXPENSES_NO_ITEMS", "You have not created any items yet");
define ("_EXPENSES_TOTAL_EX", "Total excl. VAT");
define ("_EXPENSES_TOTAL_IN", "Total incl. VAT");
define ("_EXPENSES_TOTAL_VAT", "Total VAT");
define ("_EXPENSES_CONNECT_ORDER", "Connect to order");
define ("_EXPENSES_SELECT_ORDER", "Select order or choose this one to not attach");
define ("_EXPENSES_SELECT_CUSTOMER", "Select company or choose this one to not attach");
define ("_EXPENSES_SELECT_DO_NOT", "Don't connect to an order");
define ("_EXPENSES_SELECT_DO_NOT_CUSTOMER", "Don't connect to a company");
define ("_EXPENSES_VIEW_HEADER", "Details");

// List expenses
define ("_EXPENSES_LIST_NAME", "Name");
define ("_EXPENSES_LIST_CUSTOMER", "Company");
define ("_EXPENSES_LIST_DATE", "Date");
define ("_EXPENSES_LIST_ACTIONS", "Expense actions");
define ("_EXPENSES_LIST_TYPE", "Payment");
define ("_EXPENSES_LIST_HEADER", "Expenses list");
define ("_EXPENSES_LIST_ORDER", "Order #");
define ("_EXPENSES_LIST_ACTIONS_EDIT", "Edit expense");
define ("_EXPENSES_LIST_ACTIONS_VIEW", "View expense");
define ("_EXPENSES_LIST_ACTIONS_DELETE", "Delete expense");
define ("_EXPENSES_LIST_ACTIONS_DELETE_CONFIRM", "Are you sure that you want to delete this expense ? Any files attached to it will be deleted.");
define ("_EXPENSES_ATTACHMENT", "Attachment");

?>
