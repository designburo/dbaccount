<?php

define ("_PAYMENTS_LABEL_NEW", "New");
define ("_PAYMENTS_LABEL_EDIT","Edit");
define ("_PAYMENTS_ACTION_NEW_TITLE", "Add new payment");
define ("_PAYMENTS_ACTION_EDIT_TITLE", "Edit payment");
define ("_PAYMENTS_INFO_HEADING","Payments");
define ("_PAYMENTS_INFO_TEXT","Manage payments");
define ("_PAYMENTS_INFO_NORESULTS","You have no payments defined yet.<BR>Use <i class=\"uk-icon-plus\"></i> in the panel below to add a payment.");
define ("_PAYMENTS_INFO_NEW","Use the panel below to add a new payment.");
define ("_PAYMENTS_INFO_EDIT","Use the panel below to edit.");
define ("_PAYMENTS_INFO_VIEW","Viewing");
define ("_PAYMENTS_DESCRIPTION", "Description");
define ("_PAYMENTS_DESCRIPTION_HELP", "This is additional text to describe your payment.");
define ("_PAYMENTS_DATE", "Payment Date");
define ("_PAYMENTS_TYPE", "Payment type");
define ("_PAYMENTS_NO_ORDERS", "You have no orders to connect this payment to.");
define ("_PAYMENTS_AMOUNT", "Money paid");
define ("_PAYMENTS_ORDER", "Payment for what order");
define ("_PAYMENTS_SELECT_ORDER", "Select order");
define ("_PAYMENTS_VIEW_HEADER", "Details");

// List orders
define ("_PAYMENTS_LIST_DATE", "Date");
define ("_PAYMENTS_LIST_ACTIONS", "Payment actions");
define ("_PAYMENTS_LIST_TYPE", "Payment");
define ("_PAYMENTS_LIST_HEADER", "Payments list");
define ("_PAYMENTS_LIST_DESCRIPTION", "Description");
define ("_PAYMENTS_LIST_ORDER", "Order #");
define ("_PAYMENTS_LIST_AMOUNT", "Amount");
define ("_PAYMENTS_LIST_ACTIONS_EDIT", "Edit payment");
define ("_PAYMENTS_LIST_ACTIONS_VIEW", "View payment");
define ("_PAYMENTS_LIST_ACTIONS_DELETE", "Delete payment");
define ("_PAYMENTS_LIST_ACTIONS_DELETE_CONFIRM", "Are you sure that you want to delete this payment ?");

?>
