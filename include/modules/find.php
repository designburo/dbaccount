<?php
if(!defined("MAIN")) die("No direct access");


switch ($sub) {

	case "customer" :
		$_SESSION['id']=$action;
		header( 'Location: '.DBA_URL.'customers/view.html' ) ;
		break;

	case "order" :
		$_SESSION['id']=$action;
		header( 'Location: '.DBA_URL.'orders/detail/view.html' ) ;
		break;

		case "quotes" :
			$_SESSION['id']=$action;
			header( 'Location: '.DBA_URL.'quotes/detail/view.html' ) ;
			break;

	case "expense" :
		$_SESSION['id']=$action;
		$_SESSION['action']="view";
		header( 'Location: '.DBA_URL.'expenses.html' ) ;
		break;

}
