<?php
if(!defined("MAIN")) die("No direct access");
$menu[4]['active'] = 'uk-active';
$smarty->assign('title', DBA_NAME . " -- "._CUSTOMERS);


//echo "action = ".$action;
switch ($action) {
	case "new" :
		$smarty->assign( 'action', "new");
		$smarty->assign( 'prefs', $prefs);
		$smarty->assign( 'additional_info', _CUSTOMER_INFO_NEW);
		$smarty->assign( 'screen', 'customers-home' );
		break;
	case "edit" :
		$id=$_SESSION['id'];
		$customer = $dba->db_get("*", "customer", "id", $id);
		$smarty->assign( 'action', "edit");
		$smarty->assign( 'prefs', $prefs);
		$smarty->assign( 'additional_info', "");
		$smarty->assign( 'customer', $customer[0] );
		$smarty->assign( 'screen', 'customers-home' );
		break;

	case "view" :
		$id=$_SESSION['id'];
		$customer = $dba->db_get("*", "customer", "id", $id);
		if($customer['error_status']==true) {
			$msg = _DBA_ERROR." : ".$customer['error_info'];
		} else {
			unset($customer['error_status']);
		}
		if($customer[0]['type']=="customer") {
			$cnumbers = $dba->getCustomerNumbers($id);
			$smarty->assign( 'numbers', $cnumbers );
		}
		if($customer[0]['type']=="payable") {
			$expenses = $dba->db_get("id, name, date, amount_in, amount_ex, description, vat, type, customer_id","expenses", "customer_id",$id);
			if($expenses['error_status']==true) {
				if( $expenses['error_info']=="noresult") {
					// No taxes yet
					$expenses = false;
				}	else {
					$msg = _DBA_ERROR." : ".$expenses['error_info'];
				}
			} else {
				// we have results
				unset($expenses['error_status']);
				foreach ($expenses as $key => $expense) {
					$expenses[$key]['date']=date("d-m-Y",strtotime($expense['date']) );
					if($dba->db_get("name","files","expense_id",$expense['id'],false))
					{
						$expenses[$key]['files']=true;
					} else {
						$expenses[$key]['files']=false;
					}
				}
			$smarty->assign( 'expenses', $expenses );
			}
		}
		$smarty->assign( 'prefs', $prefs );
		$smarty->assign( 'screen', 'customers-view' );
		$smarty->assign( 'customer', $customer[0] );
		$smarty->assign( 'email_template', $theme."forms/customers-email.html");
		$smarty->assign( 'additional_info', "");
		//print_r($customer[0]);
		break;
	case "" :
		$customer=$dba->db_get( "*", "customer" );
		if($customer['error_status']==true) {
			if( $customer['error_info']=="noresult" ) {
					// No customers yet
				$smarty->assign( 'screen', 'customers-home' );
				$smarty->assign( 'additional_info', _CUSTOMER_INFO_NORESULTS);
				$smarty->assign( 'action', "none");
				$smarty->assign( 'title', DBA_NAME . ' -- '._VAT );
				$action_menu=array();
				$action_menu[0]['link']=DBA_URL."customers/new.html";
				$action_menu[0]['title']=_CUSTOMER_ACTION_NEW;
				$action_menu[0]['icon']="plus";
				$smarty->assign ( 'action_menu', $action_menu);
				}	else {
					$msg = _DBA_ERROR." : ".$customer['error_info'];
				}
			} else {
			// we have results so list
				$action_menu=array();
				$action_menu[0]['link']=DBA_URL."customers/new.html";
				$action_menu[0]['title']=_CUSTOMER_ACTION_NEW;
				$action_menu[0]['icon']="plus";

				$smarty->assign ( 'action_menu', $action_menu);
				$smarty->assign( 'action', "list");
				$smarty->assign( 'prefs', $prefs);
				$smarty->assign( 'additional_info', "");
				$smarty->assign( 'screen', 'customers-list' );
				unset($customer['error_status']);
				$customer = $dba->array_sort_by_column($customer, "name");
				$smarty->assign( 'customer', $customer );

			}
			break;
	}
