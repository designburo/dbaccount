<?php
if(isset($_POST['action']) and $_POST['action']!=="")
{
	$action=$_POST['action'];
}
 else {
	 $msg.="no action defined";
 }

//echo $action;
//exit;
if($action=="new" || $action=="edit-save")
{

	if(!$comp['name']=$dba->checkPost($_POST,"name")) {
		$error .= _POST_EMPTY." "._COMPANY_NAME."<BR>";
	}

	if(!$comp['address_1']=$dba->checkPost($_POST,"address_1")) {
		$error .= _POST_EMPTY." "._COMPANY_ADDR1."<BR>";
	}

	if(!$comp['address_2']=$dba->checkPost($_POST,"address_2")) {
		$comp['address_2']=="";
	}

	if(!$comp['pcode']=$dba->checkPost($_POST,"pcode")) {
		$error .= _POST_EMPTY." "._COMPANY_ZIP."<BR>";
	}

	if(!$comp['city']=$dba->checkPost($_POST,"city")) {
		$error .= _POST_EMPTY." "._COMPANY_CITY."<BR>";
	}

	if(!$comp['country']=$dba->checkPost($_POST,"country")) {
		$comp['country']=="";
	}


	if(!$comp['phone']=$dba->checkPost($_POST,"phone")) {
		$comp['phone']=="";
	}

	if(!$comp['email']=$dba->checkPost($_POST,"email")) {
		$error .= _POST_EMPTY." "._COMPANY_EMAIL."<BR>";
	}

	if(!$comp['type']=$dba->checkPost($_POST,"type")) {
		$error .= _POST_EMPTY." "._COMPANY_TYPE."<BR>";
	}

	if(!$comp['website']=$dba->checkPost($_POST,"website")) {
		$comp['website']="";
	}

	if(!$comp['note']=$dba->checkPost($_POST,"note")) {
		$comp['note']="";
	}
}

if($action=="new") {

	if($error!=="")
	{
		$_SESSION['action']='new';
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();

	} else {
		$msg.=_POST_CUSTOMER_NEW_POST_OK."<BR>";
		$_SESSION['msg']=$msg;

		session_write_close();
		$dba->newCustomer($comp);
		//print_r($comp);
		//exit;
		header( 'Location: '.DBA_URL.'customers.html' ) ;
	}

}

if($action=="notes")
{
	if(isset($_POST['edit_note'])) {

	}
	if(isset($_POST['delete_note'])) {
		$dba->deleteNote($_POST['delete_note']);
		$msg.=_NOTE_DELETE_OK."<BR>";
		$_SESSION['msg']=$msg;
	}
	$_SESSION['id']=$_POST['id']; // so we jump back to the right customer

	session_write_close();
	header( 'Location: '.$_POST['note_return'] );

}

if($action=="edit-save") {

	if($error!=="")
	{

		$_SESSION['action']='edit';
		$_SESSION['id']=$_POST['id'];
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();
		$action="edit";
		header( 'Location: '.DBA_URL.'customers/'.$action.'.html' ) ;

	} else {
		$msg.=_POST_CUSTOMER_EDIT_POST_OK."<BR>";
		$_SESSION['msg']=$msg;
		$comp['id']=$_POST['id'];
		session_write_close();
		$dba->updateCustomer($comp);
		//print_r($comp);
		//exit;
		header( 'Location: '.DBA_URL.'customers.html' ) ;
	}
}



if($action=="detail") {

	if(isset($_POST['view']) && ($_POST['view']!=="")) {
		$action="view";
		$id=$_POST['view'];
		$sub="view";
	}
	if(isset($_POST['edit']) && ($_POST['edit']!=="")) {
		$action="edit";
		$id=$_POST['edit'];
		$sub="edit";
	}

	$_SESSION['id']=$id;
	header( 'Location: '.DBA_URL.'customers/'.$action.'.html' ) ;
	}
