<?php
if(!defined("MAIN")) die("No direct access");
$menu[7]['active'] = 'uk-active';
$smarty->assign('title', DBA_NAME . " -- "._TRANSLATE);
$action_menu=array();
$action_menu[0]['link']=DBA_URL."translate/new.html";
$action_menu[0]['title']="Make a new translation";
$action_menu[0]['icon']="plus";
$smarty->assign ( 'action_menu', $action_menu);


//echo "action = ".$action;
switch ($action) {

	case "new" :
		$smarty->assign('additional_info', "Creating a new translation file");
		$smarty->assign( 'screen', 'translate-new' );
	break;
	case "translate" :
		$language = $_SESSION['language'];
		$cat = $_SESSION['cat'];
		$additional = "<p>On the left side is the original English text. The right side has the ".$language." translation (if any).</p>";
		$additional .= "<p>Type the translations and don't forget to press save when you are done or want to finish.</p>";
		$additional .= "<p class=\"uk-text-warning uk-text-bold\">The \" (double quote) character always need to be written as \\\"  </p>";
		$smarty->assign('additional_info', $additional);
		$language = $_SESSION['language'];
		$cat = $_SESSION['cat'];
		if ($cat!="lang") {
			$english = $dba->getDefinesFromFile(DBA_PATH . "/languages/english/lang-".$cat.".php");
			$other = $dba->getDefinesFromFile(DBA_PATH . "/languages/".$language.'/lang-'.$cat.".php");
		}
		$smarty->assign( 'language', $_SESSION['language'] );
		$smarty->assign( 'screen', 'translate-step3' );
		$smarty->assign( 'cat', $cat );
		$smarty->assign( 'english', $english );
		$smarty->assign( 'other', $other );
		/*
		echo "<pre>";
		print_r($english);
		print_r($other);
		echo "</pre>";
		*/
		break;

	case "category" :
		$smarty->assign('additional_info', "Translations are done per category. Please choose what category to translate");
		$cats=$dba->directories(DBA_PATH . "/languages/".$prefs['lang'].'/', true);
		foreach ($cats as $key=>$cat) {
			$search = array (".php", "lang-");
			$replace = array ("","");
			$cats[$key]=str_replace($search, $replace,$cat);
		}
		$smarty->assign('language', $_SESSION['language']);
		$smarty->assign( 'screen', 'translate-step2' );
		$smarty->assign( 'cats', $cats );

		break;
	case "" :
	//dba_content
	$smarty->assign('additional_info', "You can either edit an existing language file (other than English) or create a new language.");
	$languages = $dba->directories(DBA_PATH . "/languages");
	foreach ($languages as $key=>$language) {
		if ($language=="english") {
			unset ($languages[$key]);
		}
	}
	if (count($languages)==0) {
		$languages=false;
	}
 /*
	echo "<pre>";
	print_r($languages);
	echo "<hr>".DBA_PATH . "/languages/".$prefs['lang']."<hr>";
	print_r( $dba->directories(DBA_PATH . "/languages/".$prefs['lang'].'/', true) );
	echo "</pre>";
*/
	$smarty->assign( 'screen', 'translate-step1' );
	$smarty->assign( 'languages', $languages );
			break;
	}
