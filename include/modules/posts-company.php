<?php
if(isset($_POST['action']) and $_POST['action']!=="")
{
	$action=$_POST['action'];
}
 else {
	 $msg.="no action defined";
 }

	if(!$comp['name']=$dba->checkPost($_POST,"name")) {
		$error .= _POST_EMPTY." "._GENERAL_NAME."<BR>";
	}

	if(!$comp['address_1']=$dba->checkPost($_POST,"address_1")) {
		$error .= _POST_EMPTY." "._GENERAL_ADDR1."<BR>";
	}

	if(!$comp['address_2']=$dba->checkPost($_POST,"address_2")) {
		$comp['address_2']=="";
	}

	if(!$comp['pcode']=$dba->checkPost($_POST,"pcode")) {
		$error .= _POST_EMPTY." "._GENERAL_ZIP."<BR>";
	}

	if(!$comp['city']=$dba->checkPost($_POST,"city")) {
		$error .= _POST_EMPTY." "._GENERAL_CITY."<BR>";
	}

	if(!$comp['country']=$dba->checkPost($_POST,"country")) {
		$error .= _POST_EMPTY." "._GENERAL_COUNTRY."<BR>";
	}

	if(!$comp['vat']=$dba->checkPost($_POST,"vat")) {
		$error .= _POST_EMPTY." "._GENERAL_VAT."<BR>";
	}

	if(!$comp['coc']=$dba->checkPost($_POST,"coc")) {
		$error .= _POST_EMPTY." "._GENERAL_COC."<BR>";
	}

	if(!$comp['bank']=$dba->checkPost($_POST,"bank")) {
		$error .= _POST_EMPTY." "._GENERAL_BANK."<BR>";
	}

	if(!$comp['swift']=$dba->checkPost($_POST,"swift")) {
		$error .= _POST_EMPTY." "._GENERAL_SWIFT."<BR>";
	}

	if(!$comp['contact']=$dba->checkPost($_POST,"contact")) {
		$error .= _POST_EMPTY." "._GENERAL_CONTACT."<BR>";
	}

	if(!$comp['phone']=$dba->checkPost($_POST,"phone")) {
		$error .= _POST_EMPTY." "._GENERAL_PHONE."<BR>";
	}

	if(!$comp['email']=$dba->checkPost($_POST,"email")) {
		$error .= _POST_EMPTY." "._GENERAL_EMAIL."<BR>";
	}

	if(!$comp['website']=$dba->checkPost($_POST,"website")) {
		$error .= _POST_EMPTY." "._GENERAL_WEBSITE."<BR>";
	}

	if(!$comp['skype']=$dba->checkPost($_POST,"skype")) {
		$comp['skype']="";
	}

	if($dba->getLogo()) {
		$comp['logo']=$dba->getLogo();
	}
	if(file_exists($_FILES["logo"]["tmp_name"]) || is_uploaded_file($_FILES['logo']['tmp_name'])) {
		if($_FILES['logo']['size']>2120000) {
			$error .= _POST_EMPTY." "._POST_COMPANY_LOGO_TOBIG."<BR>";
		} else {
			$imageData = @getimagesize($_FILES["logo"]["tmp_name"]);
			if($imageData === FALSE || !($imageData[2] == IMAGETYPE_GIF || $imageData[2] == IMAGETYPE_JPEG || $imageData[2] == IMAGETYPE_PNG)) {
				$error .= _POST_EMPTY." "._POST_COMPANY_LOGO_WRONG."<BR>";
			} else {
				$ext = pathinfo($_FILES["logo"]["name"], PATHINFO_EXTENSION);
				$f = DBA_PATH."/uploads/logo/logo.".$ext;
				move_uploaded_file($_FILES["logo"]["tmp_name"], $f);
				$comp['logo'] = DBA_URL."uploads/logo/logo.".$ext;
			}
		}

		}

if($action=="new") {

	if($error!=="")
	{
		$_SESSION['action']='new';
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();

	} else {
		$msg.=_POST_COMPANY_NEW_POST_OK."<BR>";
		$_SESSION['msg']=$msg;

		session_write_close();
		$dba->newCompany($comp);
		//print_r($comp);
		//exit;
		header( 'Location: '.DBA_URL.'preferences/company.html' ) ;
	}

}


if($action=="edit") {
	$comp['id']=$_POST['id'];

	if($error!=="")
	{
		//$_SESSION['action']='new';
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();

	} else {
		$msg.=_POST_COMPANY_EDIT_POST_OK."<BR>";
		$_SESSION['msgtype']='success';
		$_SESSION['msg']=$msg;

		session_write_close();
		$dba->updateCompany($comp);
		//print_r($comp);
		//exit;
		header( 'Location: '.DBA_URL.'preferences/company.html' ) ;
	}

}
