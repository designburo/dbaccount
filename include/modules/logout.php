<?php
if(!defined("MAIN")) die("No direct access");
unset($_SESSION['user']);
unset($_SESSION['pass']);
session_write_close();
header( 'Location: '.DBA_URL.'home.html' ) ;
