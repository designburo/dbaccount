<?php
if(!defined("MAIN")) die("No direct access");
$menu[1]['active'] = 'uk-active';

$smarty->assign( 'title', DBA_NAME . " -- "._PAYMENTS);
$action_menu=array();
$action_menu[0]['link']=DBA_URL."payments/new.html";
$action_menu[0]['title']=_PAYMENTS_ACTION_NEW_TITLE;
$action_menu[0]['icon']="plus";
$smarty->assign ( 'action_menu', $action_menu);
$smarty->assign ( 'currency', $prefs['currency']);

switch ($action) {
	case "edit" :
		$id=$_SESSION['id'];
		$get_payment = $dba->db_get("*", "payments", "id", $id, false);
		$payment=$get_payment[0];
		$payment['date']=date("Y-m-d",strtotime($payment['date']));


		$orders = $dba->db_get("id, date, description, customer_id", "orders");
		unset($orders['error_status']);


		foreach ($orders as $key=>$order) {
			$orders[$key]['id']=$order['id'];
			$orders[$key]['number']=$dba->orderNumber($order['id'], $order['date']);
			$orders[$key]['date']=date("Y-m-d",strtotime($order['date']));
			if ($order['id']==$payment['order_id']) {
				$orders[$key]['selected']="selected";
			} else {
				$orders[$key]['selected']="";
			}
			$order['description']=base64_decode($order['description']);
			$order['description']=str_replace('\r\n','',$order['description']);
			$orders[$key]['description'] = strlen($order['description']) > 50 ? substr($order['description'],0,50)."..." : $order['description'];
			$customer = $dba->db_get("name","customer","id",$order['customer_id'],false);
			$orders[$key]['customer']=$customer[0]['name'];
		}
			$smarty->assign( 'orders', $orders );
		$smarty->assign( 'payment', $payment );
		$smarty->assign( 'screen', 'payments-edit' );
		$smarty->assign( 'additional_info', _EXPENSES_INFO_NEW);


		break;
	case "new" :
		if(isset($_SESSION['id'])) {
			$oid=$_SESSION['id'];
		} else {
			$oid=false;
		}
		$orders = $dba->db_get("id, date, description, customer_id", "orders","type","order");
		if($orders['error_status']==true) {
			$connect_order=false;
			} else {
			$connect_order=true;
			unset($orders['error_status']);


		foreach ($orders as $key=>$order) {
			$orders[$key]['id']=$order['id'];
			$orders[$key]['number']=$dba->orderNumber($order['id'], $order['date']);
			$orders[$key]['date']=date("Y-m-d",strtotime($order['date']));
			$order['description']=base64_decode($order['description']);
			$order['description']=str_replace('\r\n','',$order['description']);
			$orders[$key]['description'] = strlen($order['description']) > 50 ? substr($order['description'],0,50)."..." : $order['description'];
			$customer = $dba->db_get("name","customer","id",$order['customer_id'],false);
			$orders[$key]['customer']=$customer[0]['name'];
		}
			$smarty->assign( 'orders', $orders );
			}

		$smarty->assign( 'connect_order', $connect_order );
		$smarty->assign( 'selected_order', $oid );
		$smarty->assign( 'screen', 'payments-new' );
		$smarty->assign( 'additional_info', _EXPENSES_INFO_NEW);
		break;
	case "view" :
		$id=$_SESSION['id'];
		//unset($_SESSION['id']);
		$expense = $dba->db_get("*","expenses","id",$id);
		unset($expense['error_status']);
		$file=$dba->db_get("*","files","expense_id",$id,false);
		if($file!==false) {
			$file=$file[0];
		}
		$expense=$expense[0];
		$order=$dba->db_get( 'date', "orders", "id", $expense['order_id'], false);
		if($order) {
			$expense['order']=$dba->orderNumber($expense['order_id'],$order[0]['date']);
		} else {
			$expense['order']=false;
		}
		$expense['date']=date("Y-m-d",strtotime($expense['date']));
		$smarty->assign( 'additional_info', "");
		$smarty->assign( 'screen', 'expenses-view' );
		$smarty->assign( 'file', $file );
		$smarty->assign( 'expense', $expense );
		$smarty->assign( 'prefs', $prefs);
		break;

	case "" :
		if(isset($_SESSION['id'])) {
			unset($_SESSION['id']);
		}
	$payments = $dba->db_get("*","payments");
	if($payments['error_status']==true) {
		if( $payments['error_info']=="noresult") {
			// No taxes yet
			$smarty->assign( 'additional_info', _PAYMENTS_INFO_NORESULTS);
			$smarty->assign( 'screen', 'payments-empty' );

		}	else {
			$msg = _DBA_ERROR." : ".$payments['error_info'];
		}
	} else {
		// we have results
		unset($payments['error_status']);
		foreach ($payments as $key => $payment) {
			$payments[$key]['date']=date("d-m-Y",strtotime($payment['date']) );
			$order_date= $dba->db_get("date","orders","id",$payment['order_id'],false);
			if ($order_date!==false) {
				$payments[$key]['order_nr']=$dba->orderNumber($payment['order_id'],$order_date[0]['date']);
			}
		}
		//print_r($orders);
		$smarty->assign( 'prefs', $prefs);
		$smarty->assign( 'additional_info', "");
		$smarty->assign( 'action', 'list');
		$smarty->assign( 'screen', 'payments-list' );
		$smarty->assign( 'payments', $payments );

	}
		break;
}
