<?php
	if(isset($_SESSION['id']) && ($_SESSION['id']!== "")) {
		ERROR_REPORTING(E_ALL);
		$id=$_SESSION['id'];
		include $theme."print/invoices.php";
		if(isset($_SESSION['lang'])) {
			$lang=strtoupper($_SESSION['lang']);
		} else {
			$lang=strtoupper($print_lang[0]);
		}
		$smarty->left_delimiter = '-{-';
		$smarty->right_delimiter = '-}-';
		$smarty->assign( 'theme', $theme );
		$id=$_SESSION['id'];
		$order=$dba->db_get("*", "orders", "id" ,$id,false);
		$order=$order[0];
		$order['number']=$dba->orderNumber($order['id'],$order['date']);
		$order['date']=date("d-m-Y",strtotime($order['date']));
		if ($order['description']!="") {
			$order['description']=base64_decode($order['description']);
			$order['description']=str_replace('\r\n','',$order['description']);
		} else {
			$order['description']=false;
		}
		$order['print']=$_SESSION['type'];
		if($order['print']=="print") {
			$order['font']="10px";
		} else {
			$order['font']="12px";
		}
		$order['page_width']='90%';
		if($order['print']=="pdf") {
			$order['page_width']='100%';
		}
		$customer=$dba->db_get("*", "customer", "id" ,$order['customer_id'],false);
		$customer=$customer[0];
		if($order['contacts_id']!="0") {
			$contact=$dba->db_get("fname, lname, title", "contacts", "id" ,$order['contacts_id'],false);
			$contact=$contact[0];
			$customer['contact']=$contact['title']." ".$contact['fname']." ".$contact['lname'];
		} else {
			$customer['contact']=false;
		}
		if($customer['address_2']!="") {
			$customer['address']=$customer['address_1']."<br>".$customer['address_2'];
		} else {
			$customer['address']=$customer['address_1'];
		}

		$company=$dba->db_get("*", "company", "id" ,1,false);
		$company=$company[0];
		$company['address']=$company['address_1'];
		if ($company['address_2']!=="") {
			$company['address'].="<br>".$company['address_1'];
		}
		$order['hasitems']=false;
		$qrcode="Invoice for\n".$customer['name']."\n";
		$qrcode.=$customer['address']."\n";
		$qrcode.=$customer['pcode']." ".$customer['city']."\n\n";
		$qrcode.="Invoice #".$order['number']."\n";
		$qrcode.="Invoice date : ".$order['date']."\n";
		$qrcode.="Invoice due before : ".date('d-m-Y',strtotime("+".$prefs['paydays']." day", strtotime($order['date'])))."\n\n";
		$qrcode.="Total price incl. VAT : EUR ".$order['amount_in']."\n";
		$qrcode.="Payment can be made through PayPal (creditcard and iDeal) to ".$company['email']."\n";
		$qrcode.="or\n";
		$qrcode.="By bank to ".$company['name']." at bankaccount #".$company['bank']." -- Always mention the invoice number";
		$order['qrcode']=$dba->qrcode($qrcode,400);
		$items=$dba->db_get("*", "order_details", "order_id" ,$order['id'],false);
		if($items!==false){
			$order['hasitems']=true;
		}
		$smarty->assign( 'company', $company );
		$smarty->assign( 'order', $order );
		$smarty->assign( 'customer', $customer );
		$smarty->assign( 'items', $items );
		$smarty->assign( 'prefs', $prefs );

if($order['print']=="pdf") {
	$out=$smarty->fetch('file:'.$theme.'print/invoice_'.$lang.'.php');
	require_once(DBA_PATH.'/include/tcpdf/tcpdf.php');

	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);

	$pdf->SetTitle('Factuur :#'.$order['number']);
	$pdf->SetSubject('Factuur :#'.$order['number']);
	$pdf->SetKeywords('factuur');

	// set default header data
	$pdf->SetHeaderData("logo.png", PDF_HEADER_LOGO_WIDTH, "", "");

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(15, 15, 15);
	$pdf->SetHeaderMargin(3);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 10);

	// add a page
	$pdf->AddPage();

	/* NOTE:
	 * *********************************************************
	 * You can load external XHTML using :
	 *
	 * $html = file_get_contents('/path/to/your/file.html');
	 *
	 * External CSS files will be automatically loaded.
	 * Sometimes you need to fix the path of the external CSS.
	 * *********************************************************
	 */

	// define some HTML content with style
	$html = $out;

	// output the HTML content
	$pdf->writeHTML($html, true, false, true, false, '');

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	// reset pointer to the last page
	$pdf->lastPage();

	// ---------------------------------------------------------

	//Close and output PDF document
	$filename=str_replace("/","-",$order['number']);
	/*
	$name	(string) The name of the file when saved. Note that special characters are removed and blanks characters are replaced with the underscore character.
$dest	(string) Destination where to send the document. It can take one of the following values:

		I: send the file inline to the browser (default). The plug-in is used if available. The name given by name is used when one selects the “Save as” option on the link generating the PDF.
		D: send to the browser and force a file download with the name given by name.
		F: save to a local server file with the name given by name.
		S: return the document as a string (name is ignored).
		FI: equivalent to F + I option
		FD: equivalent to F + D option
		E: return the document as base64 mime multi-part email attachment (RFC 2045)

	*/
	$pdf->Output(DBA_PATH."/uploads/pdf/".$filename.'.pdf', 'I');
	} else {
		$smarty->display('file:'.$theme.'print/invoice_'.$lang.'.php');
}
		unset($_SESSION['id']);
	}
exit;
