<?php
if(!defined("MAIN")) die("No direct access");
$menu[3]['active'] = 'uk-active';

$smarty->assign( 'title', DBA_NAME . " -- "._EXPENSES);
$action_menu=array();
$action_menu[0]['link']=DBA_URL."expenses/new.html";
$action_menu[0]['title']=_EXPENSES_ACTION_NEW_TITLE;
$action_menu[0]['icon']="plus";
$smarty->assign ( 'action_menu', $action_menu);
$smarty->assign ( 'currency', $prefs['currency']);

switch ($action) {
	case "edit" :
		$id=$_SESSION['id'];
		$get_expense = $dba->db_get("*", "expenses", "id", $id, false);
		$expense=$get_expense[0];
		$expense['date']=date("Y-m-d",strtotime($expense['date']));

		//Any attachments ?
		$get_attachment = $dba->db_get("*", "files", "expense_id", $id);
		if($get_attachment['error_status']==false) {
			$attachment=$get_attachment[0];
		} else {
			$attachment=false;
		}
		$customers = $dba->db_get("id, name, city", "customer", "type", "payable");
		if($customers['error_status']==true) {
			$doWeHaveCustomers=false;
		} else {
			$doWeHaveCustomers=true;
		unset($customers['error_status']);
		}
		$orders = $dba->db_get("id, date, description, customer_id", "orders");
		if($orders['error_status']==true) {
			$connect_order=false;
			} else {
			$connect_order=true;
			unset($orders['error_status']);


		foreach ($orders as $key=>$order) {
			$orders[$key]['id']=$order['id'];
			$orders[$key]['number']=$dba->orderNumber($order['id'], $order['date']);
			$orders[$key]['date']=date("Y-m-d",strtotime($order['date']));
			$order['description']=base64_decode($order['description']);
			$order['description']=str_replace('\r\n','',$order['description']);
			$orders[$key]['description'] = strlen($order['description']) > 50 ? substr($order['description'],0,50)."..." : $order['description'];
			$customer = $dba->db_get("name","customer","id",$order['customer_id'],false);
			$orders[$key]['customer']=$customer[0]['name'];
		}
			$smarty->assign( 'orders', $orders );
			}
			if ($doWeHaveCustomers) {
				$smarty->assign( 'customers', $customers );
			}
			$smarty->assign( 'doWeHaveCustomers', $doWeHaveCustomers );
		$smarty->assign( 'connect_order', $connect_order );
		$smarty->assign( 'expense', $expense );
		$smarty->assign( 'attachment', $attachment );
		$smarty->assign( 'screen', 'expenses-edit' );
		$smarty->assign( 'additional_info', _EXPENSES_INFO_NEW);


		break;
	case "new" :
		$customers = $dba->db_get("id, name, city", "customer", "type", "payable");
		if($customers['error_status']==true) {
			$doWeHaveCustomers=false;
		} else {
			$doWeHaveCustomers=true;
		unset($customers['error_status']);
		}
		$orders = $dba->db_get("id, date, description, customer_id", "orders");
		if($orders['error_status']==true) {
			$connect_order=false;
		} else {
			$connect_order=true;
			unset($orders['error_status']);


		foreach ($orders as $key=>$order) {
			$orders[$key]['id']=$order['id'];
			$orders[$key]['number']=$dba->orderNumber($order['id'], $order['date']);
			$orders[$key]['date']=date("Y-m-d",strtotime($order['date']));
			$order['description']=base64_decode($order['description']);
			$order['description']=str_replace('\r\n','',$order['description']);
			$orders[$key]['description'] = strlen($order['description']) > 50 ? substr($order['description'],0,50)."..." : $order['description'];
			$customer = $dba->db_get("name","customer","id",$order['customer_id'],false);
			$orders[$key]['customer']=$customer[0]['name'];
		}
			$smarty->assign( 'orders', $orders );
			}
		if ($doWeHaveCustomers) {
			$smarty->assign( 'customers', $customers );
		}
		$smarty->assign( 'doWeHaveCustomers', $doWeHaveCustomers );
		$smarty->assign( 'connect_order', $connect_order );
		$smarty->assign( 'screen', 'expenses-new' );
		$smarty->assign( 'additional_info', _EXPENSES_INFO_NEW);
		break;
	case "view" :
		$id=$_SESSION['id'];
		//unset($_SESSION['id']);
		$expense = $dba->db_get("*","expenses","id",$id);
		unset($expense['error_status']);
		$file=$dba->db_get("*","files","expense_id",$id,false);
		if($file!==false) {
			$file=$file[0];
		}
		$expense=$expense[0];
		$customer = $dba->db_get("name","customer","id",$expense['customer_id'],false);
		if($customer===false)
		{
			$expense['customer']=false;
		} else {
			$expense['customer']=$customer[0]['name'];
		}
		$order=$dba->db_get( 'date', "orders", "id", $expense['order_id'], false);
		if($order) {
			$expense['order']=$dba->orderNumber($expense['order_id'],$order[0]['date']);
		} else {
			$expense['order']=false;
		}
		$expense['date']=date("Y-m-d",strtotime($expense['date']));
		$smarty->assign( 'additional_info', "");
		$smarty->assign( 'screen', 'expenses-view' );
		$smarty->assign( 'file', $file );
		$smarty->assign( 'expense', $expense );
		$smarty->assign( 'prefs', $prefs);
		break;

	case "" :
		if(isset($_SESSION['id'])) {
			unset($_SESSION['id']);
		}
	$expenses = $dba->db_get("id, name, date, amount_in, amount_ex, description, vat, type, customer_id","expenses");
	if($expenses['error_status']==true) {
		if( $expenses['error_info']=="noresult") {
			// No taxes yet
			$smarty->assign( 'additional_info', _EXPENSES_INFO_NORESULTS);
			$smarty->assign( 'screen', 'expenses-empty' );

		}	else {
			$msg = _DBA_ERROR." : ".$expenses['error_info'];
		}
	} else {
		// we have results
		unset($expenses['error_status']);
		foreach ($expenses as $key => $expense) {
			$expenses[$key]['date']=date("d-m-Y",strtotime($expense['date']) );
			if($dba->db_get("name","files","expense_id",$expense['id'],false))
			{
				$expenses[$key]['files']=true;
			} else {
				$expenses[$key]['files']=false;
			}
			$customer = $dba->db_get("name","customer","id",$expense['customer_id'],false);
			if($customer===false)
			{
				$expenses[$key]['customer']=false;
			} else {
				$expenses[$key]['customer']=$customer[0]['name'];
			}
		}
		//print_r($orders);
		$smarty->assign( 'prefs', $prefs);
		$smarty->assign( 'additional_info', "");
		$smarty->assign( 'action', 'list');
		$smarty->assign( 'screen', 'expenses-list' );
		$expenses = $dba->array_sort_by_column($expenses, "date", SORT_DESC);
		$smarty->assign( 'expenses', $expenses );

	}
		break;
}
