<?php
if(!defined("MAIN")) die("No direct access");
$menu[7]['active'] = 'uk-active';
$smarty->assign( 'title', DBA_NAME . ' -- '._PREFS_COMPANY );
$company=$dba->db_get( "*", "company" );
if($company['error_status']==true) {
	if( $company['error_info']=="noresult" ) {
			// No company yet
		$company['name']="";
		$company['address_1']="";
		$company['address_2']="";
		$company['pcode']="";
		$company['city']="";
		$company['country']="";
		$company['email']="";
		$company['website']="";
		$company['vat']="";
		$company['coc']="";
		$company['phone']="";
		$company['contact']="";
		$company['skype']="";
		$company['bank']="";
		$company['swift']="";
		$company['id']="";
		$company['logo']="";
		$smarty->assign( 'screen', 'company' );
		$smarty->assign( 'additional_info', _COMPANY_INFO_NORESULTS);
		$smarty->assign( 'company', $company );
		$smarty->assign( 'action', "new");
		}	else {
			$msg = _DBA_ERROR." : ".$vats['error_info'];
		}
	} else {
			// we have results
			$smarty->assign( 'action', "edit");
			$smarty->assign( 'additional_info', "");
			$smarty->assign( 'screen', 'company' );
			unset($company['error_status']);
			$smarty->assign( 'company', $company[0] );
	}
