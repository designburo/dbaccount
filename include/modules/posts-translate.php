<?php
 $msg="";
$order=array();
if(isset($_POST['action']) and $_POST['action']!=="")
{
	$action=$_POST['action'];
}
 else {
	 $msg.="no action defined";
 }

if ($action=="category")
{

	if(isset($_POST['language']) && ($_POST['language']!== ""))
	{
		$_SESSION['language'] = $_POST['language'] ;
		session_write_close();
		header( 'Location: '.DBA_URL.'translate/step2/category.html' );
	} else {
		$language = false;
	}

}

if ($action=="translate")
{

	if(isset($_POST['language']) && ($_POST['language']!== ""))
	{
		$_SESSION['language'] = $_POST['language'] ;
		$_SESSION['cat'] = $_POST['cat'] ;
		session_write_close();
		header( 'Location: '.DBA_URL.'translate/step3/translate.html' );
	} else {
		$language = false;
	}

}

if ($action=="save")
{

	if(isset($_POST['language']) && ($_POST['language']!== ""))
	{
		$language = $_POST['language'] ;
		if(isset($_POST['cat']) && ($_POST['cat']!== ""))
		{
			$cat = $_POST['cat'] ;
			$defs = $dba->getDefinesFromFile(DBA_PATH . "/languages/english/lang-".$cat.".php");
      $other ="<?php\n\n";
      foreach ($defs as $k=>$def)
      {
        if($tmp=$dba->checkPost($_POST,$k)) {
          $other.="define (\"".$k."\", \"".$tmp."\")\n";
        }
      }
      $other.="\n?>\n";
      //DBA_PATH . "/languages/".$language.'/lang-'.$cat.".php"
      file_put_contents(DBA_PATH . "/languages/".$language.'/lang-'.$cat.".php", $other);
      $_SESSION['msg']=$cat." - ".$language." saved.<BR>";
    	$_SESSION['msg_type']="succes";
      session_write_close();
    	header( 'Location: '.DBA_URL.'translate.html' );
		} else {
			$msg .= "No category found<BR>";
		}
	} else {
		$msg .= "No new language name found<BR>";
	}

}
exit;
	$_SESSION['msg']=$msg;
	$_SESSION['msg_type']="warning";
	session_write_close();
	header( 'Location: '.DBA_URL.'translate.html' );
