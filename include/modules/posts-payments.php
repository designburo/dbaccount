<?php
if(isset($_POST['action']) and $_POST['action']!=="")
{
	$action=$_POST['action'];
}
 else {
	 $msg.="no action defined";
 }

//echo $action;
//exit;
if($action=="new" || $action=="edit-save")
{
	$payment=array();

	if(!$payment['date']=$dba->checkPost($_POST,"date")) {
		$error .= _POST_EMPTY." "._PAYMENTS_DATE."<BR>";
	}

	if(!$payment['order_id']=$dba->checkPost($_POST,"order_id")) {
		$error .= _POST_EMPTY."<BR>";
	}

	if(!$payment['note']=$dba->checkPost($_POST,"note")) {
		$payment['note']="";
	} else {
		$payment['note']=str_replace('\r\n','',$payment['note']);
	}

	if(!$payment['type']=$dba->checkPost($_POST,"type")) {
		$error .= _POST_EMPTY." "._PAYMENTS_TYPE."<BR>";
	}

	if(!$payment['amount']=$dba->checkPost($_POST,"amount")) {
		$error .= _POST_EMPTY." "._PAYMENTS_AMOUNT."<BR>";
	}
}

if($action=="new") {

	if($error!=="")
	{
		$_SESSION['action']='new';
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();

	} else {
		$msg.=_POST_PAYMENTS_NEW_POST_OK."<BR>";
		$_SESSION['msg']=$msg;
		$dba->newPayment($payment);

		session_write_close();
		header( 'Location: '.DBA_URL.'payments.html' ) ;
	}

}


if($action=="edit-save") {

	if($error!=="")
	{

		$_SESSION['action']='edit';
		$_SESSION['id']=$_POST['id'];
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();
		header( 'Location: '.DBA_URL.'payments.html' ) ;

	} else {
		$msg.=_POST_PAYMENTS_EDIT_POST_OK."<BR>";
		$_SESSION['msg']=$msg;
		$payment['id']=$_POST['id'];
		session_write_close();

		$dba->updatePayment($payment);
		//print_r($comp);
		//exit;
		header( 'Location: '.DBA_URL.'payments.html' ) ;
	}
}



if($action=="detail") {

	if(isset($_POST['view']) && ($_POST['view']!=="")) {
		$_SESSION['id']=$_POST['view'];
		$_SESSION['action']="view";
		session_write_close();
		header( 'Location: '.DBA_URL.'payments.html' ) ;
	}
	if(isset($_POST['edit']) && ($_POST['edit']!=="")) {
		$_SESSION['action']="edit";
		$_SESSION['id']=$_POST['edit'];
		session_write_close();
		header( 'Location: '.DBA_URL.'payments.html' ) ;
	}
	if(isset($_POST['delete']) && ($_POST['delete']!=="")) {
		$action="";
		$id=$_POST['delete'];
		$dba->deletePayment($id);
		$msg = _POST_PAYMENTS_DELETED."<br>";
		$_SESSION['msg']=$msg;
		session_write_close();
		header( 'Location: '.DBA_URL.'payments.html' ) ;
	}

	$_SESSION['id']=$id;
	header( 'Location: '.DBA_URL.'payments/'.$action.'.html' ) ;
	}
