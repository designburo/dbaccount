<?php
if(!defined("MAIN")) die("No direct access");
$menu[7]['active'] = 'uk-active';

$smarty->assign( 'title', DBA_NAME . ' -- '._ITEMS );
$action_menu=array();
$action_menu[0]['link']=DBA_URL."preferences/items/new.html";
$action_menu[0]['title']=_ITEMS_ACTION_NEW_TITLE;
$action_menu[0]['icon']="plus";
$smarty->assign ( 'action_menu', $action_menu);

switch ($action) {
	case "edit" :
		$items = $dba->db_get("*", "items", "id", $id);
		$item['name']=$items[0]['name'];
		$item['amount']=$items[0]['amount'];
		$item['vat_id']=$items[0]['vat_id'];
		$item['description']=$items[0]['description'];
		$item['id']=$id;

		$vats = $dba->db_get("*", "vat");
		unset($vats['error_status']);

		$smarty->assign( 'additional_info', _ITEMS_INFO_EDIT);
		$smarty->assign( 'action', 'edit');
		$smarty->assign( 'screen', 'items' );
		$smarty->assign( 'vats', $vats );
		$smarty->assign( 'item', $item );
		break;
	case "new" :
		// first check if we have vats
		$vats = $dba->db_get("*", "vat");
		if($vats['error_status']==true) {
			if( $vats['error_info']=="noresult") {
			// No taxes yet
				$info = _ITEMS_NO_VAT.'<a href="'.DBA_URL.'preferences/taxes/new.html" class="uk-button uk-button-primary">';
				$info .= _ITEMS_NO_VAT_LINK.'</a>';
				$smarty->assign( 'additional_info', $info);
				$smarty->assign('novats',true);
			}	else {
				$msg = _DBA_ERROR." : ".$vats['error_info'];
			}
		} else {
			// we have results
			unset($vats['error_status']);
			$smarty->assign( 'additional_info', _ITEMS_INFO_NEW);
			$smarty->assign( 'vats', $vats );
			$smarty->assign('novats',false);
		}

		$smarty->assign( 'action', 'new');
		$smarty->assign( 'screen', 'items' );
		break;
	case "" :
	$items = $dba->db_get("*", "items");
	if($items['error_status']==true) {
		if( $items['error_info']=="noresult") {
			// No taxes yet
			$smarty->assign( 'additional_info', _ITEMS_INFO_NORESULTS);
			$smarty->assign( 'action', 'empty');
			$smarty->assign( 'screen', 'items' );

		}	else {
			$msg = _DBA_ERROR." : ".$items['error_info'];
		}
	} else {
		// we have results
		$items=$dba->addVATNames($items);

		$smarty->assign( 'additional_info', "");
		$smarty->assign( 'action', 'list');
		$smarty->assign( 'screen', 'items' );
		unset($items['error_status']);
		$smarty->assign( 'items', $items );
	}
		break;
}
