<?php
$_SESSION['id']=$_POST['id'];
$mail['id']=$_POST['id'];
$mail['cid']=0;
$mail['type']=$_POST['mailtype'];
error_reporting( E_ALL );
if(isset($_POST['return']) && ($_POST['return']!== "")) {
	$mail['return']=$dba->db_real_escape($_POST['return']);
} else {
	$error .= _POST_EMAIL_ERROR_RETURN."<BR>";
}

if(isset($_POST['mailname']) && ($_POST['mailname']!== "")) {
	$mail['mailname']=$dba->db_real_escape($_POST['mailname']);
} else {
	$error .= _POST_EMAIL_ERROR_TO." [name]<BR>";
}

if(isset($_POST['mailto']) && ($_POST['mailto']!== "")) {
	$mail['mailto']=$dba->db_real_escape($_POST['mailto']);
} else {
	$error .= _POST_EMAIL_ERROR_TO."<BR>";
}

if(isset($_POST['mailsubject']) && ($_POST['mailsubject']!== "")) {
	$mail['mailsubject']=$dba->db_real_escape($_POST['mailsubject']);
} else {
	$error .= _POST_EMAIL_ERROR_SUBJECT."<BR>";
}

if(isset($_POST['mailbody']) && ($_POST['mailbody']!== "")) {
	$mail['mailbody']=$_POST['mailbody'];
} else {
	$error .= _POST_EMAIL_ERROR_BODY."<BR>";
}

if($error!=="")
{
	$msg.=$error."<BR>"._POST_ERRORS."<BR>";
	$_SESSION['msg']=$msg;
	$_SESSION['msgtype']="warning";
	session_write_close();
	//print_r($_POST);
	//echo "error:".$error; exit;
	header( 'Location: '.$mail['return'] ) ;

} else {


	require DBA_PATH.'/include/PHPMailer/PHPMailerAutoload.php';
	//$m = new PHPMailer;
	$send=$dba->sendMail($mail);
	if (!$send['error_status']){
		$msg.= $send['error_info'];
		$_SESSION['msgtype']="warning";
	} else {
		$msg.=_POST_EMAIL_SEND_OK."<BR>";
		$_SESSION['msgtype']="success";
	}
		$_SESSION['msg']=$msg;

	session_write_close();

	header( 'Location: '.$mail['return'] ) ;
}


