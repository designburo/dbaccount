<?php
if(!defined("MAIN")) die("No direct access");
$menu[7]['active'] = 'uk-active';

$smarty->assign( 'title', DBA_NAME . ' -- '._VAT );
$action_menu=array();
$action_menu[0]['link']=DBA_URL."preferences/vats/new.html";
$action_menu[0]['title']=_VAT_ACTION_NEW;
$action_menu[0]['icon']="plus";
$smarty->assign ( 'action_menu', $action_menu);

switch ($action) {
	case "edit" :
		$vats = $dba->db_get("*", "vat", "id", $id);
		$vat['name']=$vats[0]['name'];
		$vat['vat']=$vats[0]['vat'];
		$vat['id']=$id;
		$smarty->assign( 'additional_info', _VAT_INFO_EDIT);
		$smarty->assign( 'action', 'edit');
		$smarty->assign( 'screen', 'vat' );
		$smarty->assign( 'vat', $vat );
		break;
	case "new" :
		$smarty->assign( 'additional_info', _VAT_INFO_NEW);
		$smarty->assign( 'action', 'new');
		$smarty->assign( 'screen', 'vat' );
		break;
	case "" :
	$vats = $dba->db_get("*", "vat");
	if($vats['error_status']==true) {
		if( $vats['error_info']=="noresult") {
			// No taxes yet
			$smarty->assign( 'additional_info', _VAT_INFO_NORESULTS);
			$smarty->assign( 'action', 'empty');
			$smarty->assign( 'screen', 'vat' );

		}	else {
			$msg = _DBA_ERROR." : ".$vats['error_info'];
		}
	} else {
		// we have results
		$smarty->assign( 'additional_info', "");
		$smarty->assign( 'action', 'list');
		$smarty->assign( 'screen', 'vat' );
		unset($vats['error_status']);
		$smarty->assign( 'vats', $vats );
	}
		break;
}
