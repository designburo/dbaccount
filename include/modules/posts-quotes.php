<?php


/*
order_date
order_customer
order_description
order_internal_note

order_item_amount[]
order_item_description[]
order_item_vat[] %
order_item_price[] item price
order_item_total[] total price

order_total_ex
order_total_vat
order_total_in

*/

$order=array();
if(isset($_POST['action']) and $_POST['action']!=="")
{
	$action=$_POST['action'];
}
 else {
	 $msg.="no action defined";
 }

if ($action=="new" || $action=="edit-save")
{

	if(isset($_POST['date']) && ($_POST['date']!== ""))
	{
		$order['date']=$dba->db_real_escape($_POST['date']);
	} else {
		$error .= _POST_QUOTES_EMPTY_DATE;
	}

	if(isset($_POST['customer_id']) && ($_POST['customer_id']!== "")) {
		$order['customer_id']=$_POST['customer_id'];
	} else {
		$error .= _POST_QUOTES_EMPTY_CUSTOMER; //order_contact
	}

	if(isset($_POST['order_contact']) && ($_POST['order_contact']!== "")) {
		$order['contacts_id']=$_POST['order_contact'];
	} else {
		$order['contacts_id']=0;
	}

	if(isset($_POST['description']) && ($_POST['description']!== "")) {
		$order['description']=base64_encode($_POST['description']);
	} else {
		$order['description']="";
	}

	if(isset($_POST['description_internal']) && ($_POST['description_internal']!== "")) {
		$order['description_internal']=$dba->db_real_escape($_POST['description_internal']);
	} else {
		$order['description_internal']="";
	}

	if(isset($_POST['order_total_ex']) && ($_POST['order_total_ex']!== "")) {
		$order['amount_ex']=$dba->db_real_escape($_POST['order_total_ex']);
	} else {
		$error .= _POST_QUOTES_EMPTY_TOTALEX;
	}

	if(isset($_POST['order_total_in']) && ($_POST['order_total_in']!== "")) {
		$order['amount_in']=$dba->db_real_escape($_POST['order_total_in']);
	} else {
		$error .= _POST_QUOTES_EMPTY_TOTALEX;
	}

	if(isset($_POST['order_total_vat']) && ($_POST['order_total_vat']!== "")) {
		$order['vat']=$dba->db_real_escape($_POST['order_total_vat']);
	} else {
		$error .= _POST_QUOTES_EMPTY_VAT;
	}

	if(isset($_POST['order_item_amount']) && ($_POST['order_item_amount']!== "")) {

		$item_amounts=$_POST['order_item_amount'];
		$item_description=$_POST['order_item_description'];
		$item_vat=$_POST['order_item_vat'];
		$item_price=$_POST['order_item_price'];
		$item_total=$_POST['order_item_total'];
		$items=array();
		foreach ( $item_amounts as $key => $amount ) {
			if ($item_description[$key]!=="") {
			$items[$key]['amount']=$dba->db_real_escape($amount);
			$items[$key]['item_name']=$dba->db_real_escape($item_description[$key]);
			$items[$key]['vat']=($dba->db_real_escape($item_total[$key])/100)*($dba->db_real_escape($item_vat[$key]));
			$items[$key]['price']=$dba->db_real_escape($item_price[$key]);
			$items[$key]['price_total']=$dba->db_real_escape($item_total[$key]);
			}
		}
	} else {
			$items=array();
	}
/*
	echo "<pre>";
	print_r($order);
	print_r($items);
	echo "</pre>";
	//exit;
	*/
}
if($action=="detail") {
	if (isset($_POST['view'])){
		$_SESSION['id']=$_POST['view'];
		header( 'Location: '.DBA_URL.'orders/detail/view.html' ) ;
	}
	if (isset($_POST['print'])){
		$_SESSION['id']=$_POST['print'];
		if(isset($_POST['return']) && $_POST['return']!=""){
			$_SESSION['return']=$_POST['return'];
		}
		header( 'Location: '.DBA_URL.'orders/detail/print.html' ) ;
	}
	if (isset($_POST['pdf'])){
		$_SESSION['id']=$_POST['pdf'];
		header( 'Location: '.DBA_URL.'orders/detail/pdf.html' ) ;
	}
	if (isset($_POST['payment'])){
		$_SESSION['id']=$_POST['payment'];
		header( 'Location: '.DBA_URL.'payments/new.html' ) ;
	}
}

if(isset($_POST['delete']) && ($_POST['delete']!=="")) {
		$action="";
		$id=$_POST['delete'];
		$dba->deleteOrder($id);
		$msg = _POST_QUOTES_DELETE."<br>";
		$_SESSION['msg']=$msg;
		session_write_close();
		header( 'Location: '.DBA_URL.'orders.html' ) ;
	}

if($action=="detail") {
	if (isset($_POST['edit'])){
		$_SESSION['id']=$_POST['edit'];
		header( 'Location: '.DBA_URL.'orders/detail/edit.html' ) ;
	}
}


if($action=="new") {

	if($error!=="") {
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
	} else {
		$msg.=_POST_QUOTES_NEW_POST_OK."<BR>";
		$_SESSION['msg']=$msg;
		session_write_close();
		$order['type']="quote";
		$dba->newOrder($order, $items);
		header( 'Location: '.DBA_URL.'orders.html' ) ;
	}
}

if($action=="edit-save") {
		$order['id']=$_POST['id'];
		if(isset($_POST['delete'])) {
			$dba->deleteOrder($items['id']);
			$msg.=_POST_QUOTES_DELETE."<br>";
			$_SESSION['msg']=$msg;
			session_write_close();
			header( 'Location: '.DBA_URL.'orders.html' ) ;
		}
	if($error!=="") {
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
	} else {
		$msg.=_POST_QUOTES_EDIT_POST_OK."<BR>";
		$_SESSION['msg']=$msg;
		$_SESSION['msgtype']="success";
		session_write_close();
		$dba->updateOrder($order, $items);
		header( 'Location: '.DBA_URL.'orders.html' ) ;
	}
}
