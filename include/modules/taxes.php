<?php
if(!defined("MAIN")) die("No direct access");
$menu[6]['active'] = 'uk-active';

$smarty->assign( 'title', DBA_NAME . " -- "._TAXES);
$action_menu=array();
$action_menu[0]['link']=DBA_URL."expenses/new.html";
$action_menu[0]['title']=_EXPENSES_ACTION_NEW_TITLE;
$action_menu[0]['icon']="plus";
$smarty->assign ( 'action_menu', $action_menu);
$smarty->assign ( 'currency', $prefs['currency']);

switch ($action) {
	

	case "" :
		if(isset($_SESSION['id'])) {
			unset($_SESSION['id']);
		}
		$years = $dba->get_years_used();
		if (empty($years)) {
			$years=false;
		}
		$current_year = date("Y");
		if($years!==false) {
			$smarty->assign( 'years', $years );
			$smarty->assign( 'current_year', $current_year );
			$smarty->assign( 'additional_info', "");
		} else {
			$smarty->assign( 'years', $years );
			$smarty->assign( 'additional_info', _TAXES_INFO_NORESULTS);
		}
		$smarty->assign( 'screen', 'taxes' );

		break;
}
