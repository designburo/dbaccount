<?php
	if(!defined("MAIN")) die("No direct access");
$menu[7]['active'] = 'uk-active';
$smarty->assign( 'themes', $dba->available_themes );
$smarty->assign( 'languages', $dba->available_languages );
$smarty->assign( 'colorschemes', $dba->available_colorschemes );
$smarty->assign( 'currencies', $dba->currencies );
$smarty->assign( 'title', DBA_NAME . ' -- '._PREFERENCES );
$smarty->assign( 'screen', 'prefs' );
$smarty->assign( 'language', $prefs['lang'] );
$smarty->assign( 'theme_name', $prefs['theme'] );
$smarty->assign( 'appname', $prefs['name'] );
$smarty->assign( 'currency', $prefs['currency'] );
$smarty->assign( 'timezone', $prefs['timezone'] );
$smarty->assign( 'paydays', $prefs['paydays'] );
$smarty->assign( 'colorscheme', $prefs['colorscheme'] );
$smarty->assign( 'order_number', $prefs['order_number'] );
$smarty->assign( 'psmtp', $dba->smtp );
//print_r($dba->smtp);
