<?php
switch($page)
{
	case "find":
		include (DBA_MODULES."find.php");

		break;
	case "print":
		include (DBA_MODULES."print.php");

		break;
	case "home":

		error_reporting(0);
		include (DBA_MODULES."dashboard.php");
		break;
	case "orders":
		if ($sub=="new") {
			$action="new";
		}

		include (DBA_MODULES."orders.php");
		break;
	case "quotes":
	if ($sub=="new") {
		$action="new";
	}

	include (DBA_MODULES."quotes.php");
	break;
	case "expenses":
		if ($sub=="new") {
			$action="new";
		}
		if ($sub=="view") {
			$action="view";
		}
		include (DBA_MODULES."expenses.php");
		break;
	case "payments":
		if ($sub=="new") {
			$action="new";
		}
		if ($sub=="view") {
			$action="view";
		}
		include (DBA_MODULES."payments.php");
		break;
	case "taxes":
		include (DBA_MODULES."taxes.php");
		break;
	case "translate":
		include (DBA_MODULES."translate.php");
		break;

	case "customers": // done cause two argument in url ain't working. three is!
		if ($sub=="new") {
			$action="new";
		}
		if ($sub=="detail") {
			$action="detail";
		}
		if ($sub=="edit") {
			$action="edit";
		}
		if ($sub=="view") {
			$action="view";
		}
		include (DBA_MODULES."customers.php");
		break;
	case "contacts":
		if ($sub=="new") {
			$action="new";
		}
		if ($sub=="detail") {
			$action="detail";
		}
		if ($sub=="edit") {
			$action="edit";
		}
		if ($sub=="view") {
			$action="view";
		}
		include (DBA_MODULES."contacts.php");
		break;
	case "preferences":
		if ($sub) {
			switch ($sub)
			{
				case "company" :
					include (DBA_MODULES."company.php");
					break;
				case "vats":
					include (DBA_MODULES."vats.php");
					break;
				case "settings":
					include (DBA_MODULES."settings.php");
					break;
				case "items":
					include (DBA_MODULES."items.php");
					break;
				case "logout":
					include (DBA_MODULES."logout.php");
					break;
			}
		}
			break;

	default:
		$msg= _MSG_STATUS." : "._UNKNOWN." page<br>";
		$msgtype="danger";
		$smarty->assign('title', DBA_NAME . " -- "._UNKNOWN);
}
