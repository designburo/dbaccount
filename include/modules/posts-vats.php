<?php
if(isset($_POST['action']) and $_POST['action']!=="")
{
	$action=$_POST['action'];
}
 else {
	 $msg.="no action defined";
 }

if($action=="new") {
	if(isset($_POST['vatname']) && ($_POST['vatname']!== ""))
	{
		$vat['name']=$dba->db_real_escape($_POST['vatname']);
	} else {
		$error .= _POST_VAT_EMPTYNAME."<BR>";
	}

	if(isset($_POST['vatvalue']) && ($_POST['vatvalue']!== "") && (is_numeric($_POST['vatvalue']))) {
		$vat['value']=$_POST['vatvalue'];
	} else {
		$error .= _POST_VAT_ERRORVALUE."<BR>";
	}

	if($error!=="")
	{
		$_SESSION['action']='new';
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();

	} else {
		$msg.=_POST_VAT_NEW_POST_OK."<BR>";
		$_SESSION['msg']=$msg;

		session_write_close();
		$dba->newVAT($vat);
		//print_r($prefs);
		header( 'Location: '.DBA_URL.'preferences/vats.html' ) ;
	}
}
if($action=="edit") {
	$id=$_POST['id'];
}
if($action=="edit-save") {
	$vat['id']=$_POST['id'];
		if(isset($_POST['delete'])) {
		$dba->deleteVAT($vat['id']);
		$msg.=_POST_VAT_DELETE."<br>";
		$_SESSION['msg']=$msg;
		session_write_close();
		header( 'Location: '.DBA_URL.'preferences/vats.html' ) ;
	}
	if(isset($_POST['vatname']) && ($_POST['vatname']!== ""))
	{
		$vat['name']=$dba->db_real_escape($_POST['vatname']);
	} else {
		$error .= _POST_VAT_EMPTYNAME."<BR>";
	}

	if(isset($_POST['vatvalue']) && ($_POST['vatvalue']!== "") && (is_numeric($_POST['vatvalue']))) {
		$vat['value']=$_POST['vatvalue'];
	} else {
		$error .= _POST_VAT_ERRORVALUE."<BR>";
	}

	if($error!=="")
	{
		//$_SESSION['action']='new';
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();

	} else {
		$msg.=_POST_VAT_EDIT_POST_OK."<BR>";
		$_SESSION['msg']=$msg;

		session_write_close();
		$dba->updateVat($vat);
		//print_r($vat);
		//exit;
		header( 'Location: '.DBA_URL.'preferences/vats.html' ) ;
	}

}

