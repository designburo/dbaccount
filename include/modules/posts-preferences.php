<?php

if(isset($_POST['appname']) && ($_POST['appname']!== ""))
{
	$prefs['name']=$dba->db_real_escape($_POST['appname']);
} else {
	$error .= _POST_PREFS_EMPTYNAME;
}

if(isset($_POST['theme']) && ($_POST['theme']!== "")) {
	$prefs['theme']=$_POST['theme'];
} else {
	$prefs['theme']="default";
}

if(isset($_POST['colorscheme'])) {
	$prefs['colorscheme']=$_POST['colorscheme'];
} else {
	$prefs['colorscheme']=".almost-flat";
}

if(isset($_POST['language']) && ($_POST['language']!== "")) {
	$prefs['lang']=$_POST['language'];
} else {
	$prefs['lang']="english";
}

if(isset($_POST['currency']) && ($_POST['currency']!== "")) {
	$prefs['currency']=$_POST['currency'];
} else {
	$prefs['currency']="euro";
}
if(isset($_POST['timezone']) && ($_POST['timezone']!== "")) {
	$prefs['timezone']=$_POST['timezone'];
} else {
	$prefs['timezone']="Europe/Amsterdam";
	$msg.="No timezone found set  to : Europe/Amsterdam<br>";
}

if(isset($_POST['paydays']) && ($_POST['paydays']!== "")) {
	$prefs['paydays']=$_POST['paydays'];
} else {
	$prefs['paydays']="7";
	$msg.="No paydays found set  to : 7<br>";
}

if(isset($_POST['order_number']) && ($_POST['order_number']!== "")) {
	$prefs['order_number']=$_POST['order_number'];
} else {
	$prefs['order_number']="1";
	$msg.="No order presentation found set to : 1<br>";
}

$mail = array("smtp_host", "smtp_port", "smtp_user", "smtp_pass");
foreach($mail as $m) {
		if(isset($_POST[$m]) && ($_POST[$m]!== "")) {
			$dat[$m]=$_POST[$m];
	} else {
			$dat[$m]="0";
	}
}
if(isset($_POST['smtp_auth'])) {
	$dat['smtp_auth']=true;
} else {
		$dat['smtp_auth']=false;
}

if($error!=="") {
	$msg.=$error."<BR>"._POST_ERRORS."<BR>";
} else {
	$msg.=_POST_PREFS_POST_OK."<BR>";
	$_SESSION['msg']=$msg;
	session_write_close();
	saveSettings($prefs);
	$dba->saveSMTP($dat);
	//print_r($prefs);
	//exit;
	header( 'Location: '.DBA_URL.'preferences/settings.html' ) ;
}

