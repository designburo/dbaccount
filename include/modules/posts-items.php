<?php

if(isset($_POST['action']) and $_POST['action']!=="")
{
	$action=$_POST['action'];
}
 else {
	 $msg.="no action defined";
 }

if(isset($_POST['itemname']) && ($_POST['itemname']!== ""))
{
	$items['name']=$dba->db_real_escape($_POST['itemname']);
} else {
	$error .= _POST_ITEMS_EMPTY_NAME;
}

if(isset($_POST['itemprice']) && ($_POST['itemprice']!== "")) {
	$items['amount']=$_POST['itemprice'];
} else {
	$error .= _POST_ITEMS_EMPTY_PRICE;
}

if(isset($_POST['itemvat']) && ($_POST['itemvat']!== "")) {
	$items['vat_id']=$_POST['itemvat'];
} else {
	$error .= _POST_ITEMS_EMPTY_VAT;
}

if(isset($_POST['itemdescription']) && ($_POST['itemdescription']!== "")) {
	$items['description']=$_POST['itemdescription'];
} else {
	$items['description']="";
}

if($action=="new") {

	if($error!=="") {
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
	} else {
		$msg.=_POST_ITEMS_NEW_POST_OK."<BR>";
		$_SESSION['msg']=$msg;
		session_write_close();
		$dba->newItem($items);
		header( 'Location: '.DBA_URL.'preferences/items.html' ) ;
	}
}
if($action=="edit") {
	$id=$_POST['id'];
}
if($action=="edit-save") {
		$items['id']=$_POST['id'];
		if(isset($_POST['delete'])) {
		$dba->deleteItem($items['id']);
		$msg.=_POST_ITEMS_DELETE."<br>";
		$_SESSION['msg']=$msg;
		session_write_close();
		header( 'Location: '.DBA_URL.'preferences/items.html' ) ;
		}
	if($error!=="") {
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
	} else {
		$msg.=_POST_ITEMS_EDIT_POST_OK."<BR>";
		$_SESSION['msg']=$msg;
		$_SESSION['msgtype']="success";
		session_write_close();
		$dba->updateItem($items);
		header( 'Location: '.DBA_URL.'preferences/items.html' ) ;
	}
}

