<?php
if(isset($_POST['action']) and $_POST['action']!=="")
{
	$action=$_POST['action'];
}
 else {
	 $msg.="no action defined";
 }

//echo $action;
//exit;
if($action=="new" || $action=="edit-save")
{
	$expense=array();

	if(!$expense['date']=$dba->checkPost($_POST,"date")) {
		$error .= _POST_EMPTY." "._EXPENSES_DATE."<BR>";
	}

	if(!$expense['name']=$dba->checkPost($_POST,"name")) {
		$error .= _POST_EMPTY." "._EXPENSES_NAME."<BR>";
	}

	if(!$expense['description']=$dba->checkPost($_POST,"description")) {
		$error .= _POST_EMPTY." "._EXPENSES_DESCRIPTION."<BR>";
	} else {
		$expense['description']=str_replace('\r\n','',$expense['description']);
	}

		if(!$expense['type']=$dba->checkPost($_POST,"type")) {
		$error .= _POST_EMPTY." "._EXPENSES_TYPE."<BR>";
	}

	if(!$expense['invoice']=$dba->checkPost($_POST,"invoice")) {
		$expense['invoice']="";
	}
	if(!$expense['customer_id']=$dba->checkPost($_POST,"customer_id")) {
		$expense['customer_id']=0;
	}

	if(!$expense['amount_ex']=$dba->checkPost($_POST,"amount_ex")) {
		$error .= _POST_EMPTY." "._EXPENSES_TOTAL_EX."<BR>";
	}

	if(!$expense['order_id']=$dba->checkPost($_POST,"order_id")) {
		$expense['order_id']=0;
	}

	if(!$expense['amount_in']=$dba->checkPost($_POST,"amount_in")) {
		$error .= _POST_EMPTY." "._EXPENSES_TOTAL_IN."<BR>";
	}

	if(!$expense['vat']=$dba->checkPost($_POST,"vat")) {
		$expense['vat']=0;
	}
}
if($action=="new") {
	if(file_exists($_FILES["files"]["tmp_name"]) || is_uploaded_file($_FILES['files']['tmp_name'])) {
		if(!$bestandsnaam=$dba->checkPost($_POST,"files_name")) {
			$error .= _POST_EMPTY." "._POST_EXPENSES_FILE_NONAME."<BR>";
		}
	}
}
if($action=="edit-save") {
	if(file_exists($_FILES["files"]["tmp_name"]) || is_uploaded_file($_FILES['files']['tmp_name'])) {
		$bestandsnaam=$dba->checkPost($_POST,"files_name");
	}
	if(!$files['id']=$dba->checkPost($_POST,"files_id")) {
		$error .= _POST_EMPTY." "._EXPENSES_NO_FILE_ID."<BR>";
	}
	if(!$files['name']=$dba->checkPost($_POST,"files_name")) {
		$files['name']="";
	}
	if(!$files['file']=$dba->checkPost($_POST,"files_old")) {
		$files['file']="";
	}
	/*
	echo "<pre>";
	print_r($_POST);
	print_r($_FILES);
	echo "</pre>";
	exit;
	*/
}


if($action=="new") {

	if($error!=="")
	{
		$_SESSION['action']='new';
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();

	} else {
		$msg.=_POST_EXPENSES_NEW_POST_OK."<BR>";
		$_SESSION['msg']=$msg;

		$eid = $dba->newExpense($expense);


		if(file_exists($_FILES["files"]["tmp_name"]) || is_uploaded_file($_FILES['files']['tmp_name'])) {
			if($_FILES['files']['size']>5300000) {
				$error .= _POST_EMPTY." "._POST_EXPENSES_FILE_TOBIG."<BR>";
			} else {
				$ext = pathinfo($_FILES["files"]["name"], PATHINFO_EXTENSION);
				$allowed = array('png', 'jpg', 'tiff', 'pdf');
				if(!in_array($ext,$allowed)) {
					$error .= _POST_EMPTY." "._POST_EXPENSES_FILE_WRONG."<BR>";
				} else {
					$year = date("Y");
					$path = DBA_PATH."/uploads/files/".$year;
					$path_db="uploads/files/".$year;
					if(!file_exists($path))
					{
						mkdir($path);
						chmod($path, 0755);
					}
					$f = $path."/expense_".$eid.".".$ext;
					move_uploaded_file($_FILES["files"]["tmp_name"], $f);
					$data['file']=$path_db."/expense_".$eid.".".$ext;;
					$data['expense_id']=$eid;
					$data['name']=$bestandsnaam;
					$dba->newFile($data);
				}
			}
		}		//print_r($comp);
		//exit;
		session_write_close();
		header( 'Location: '.DBA_URL.'expenses.html' ) ;
	}

}


if($action=="edit-save") {

	if($error!=="")
	{

		$_SESSION['action']='edit';
		$_SESSION['id']=$_POST['id'];
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();
		header( 'Location: '.DBA_URL.'expenses.html' ) ;

	} else {
		$msg.=_POST_EXPENSES_EDIT_POST_OK."<BR>";
		$_SESSION['msg']=$msg;
		$expense['id']=$_POST['id'];
		session_write_close();
		if(file_exists($_FILES["files"]["tmp_name"]) || is_uploaded_file($_FILES['files']['tmp_name'])) {
			if($_FILES['files']['size']>5300000) {
				$error .= _POST_EMPTY." "._POST_EXPENSES_FILE_TOBIG."<BR>";
			} else {
				$ext = pathinfo($_FILES["files"]["name"], PATHINFO_EXTENSION);
				$allowed = array('png', 'jpg', 'tiff', 'pdf');
				if(!in_array($ext,$allowed)) {
					$error .= _POST_EMPTY." "._POST_EXPENSES_FILE_WRONG."<BR>";
				} else {
					$year = date("Y");
					$path = DBA_PATH."/uploads/files/".$year;
					$path_db = "uploads/files/".$year;
					if(!file_exists($path))
					{
						mkdir($path);
						chmod($path, 0755);
					}
					$f = $path."/expense_".$expense['id'].".".$ext;
					//move_uploaded_file($_FILES["files"]["tmp_name"], $f);
					$files['file']=$path_db."/expense_".$eid.".".$ext;
					$files['expense_id']=$expense['id'];

					//$dba->updateFile($files);
				}
			}
		}

		$dba->updateFile($files);
		$dba->updateExpense($expense);
		//print_r($comp);
		//exit;
		header( 'Location: '.DBA_URL.'expenses.html' ) ;
	}
}



if($action=="detail") {

	if(isset($_POST['view']) && ($_POST['view']!=="")) {
		$_SESSION['id']=$_POST['view'];
		$_SESSION['action']="view";
		session_write_close();
		header( 'Location: '.DBA_URL.'expenses.html' ) ;
	}
	if(isset($_POST['edit']) && ($_POST['edit']!=="")) {
		$_SESSION['action']="edit";
		$_SESSION['id']=$_POST['edit'];
		session_write_close();
		header( 'Location: '.DBA_URL.'expenses.html' ) ;
	}
	if(isset($_POST['delete']) && ($_POST['delete']!=="")) {
		$action="";
		$id=$_POST['delete'];
		$dba->deleteExpense($id);
		$msg = _POST_EXPENSES_DELETED."<br>";
		$_SESSION['msg']=$msg;
		session_write_close();
		header( 'Location: '.DBA_URL.'expenses.html' ) ;
	}

	$_SESSION['id']=$id;
	header( 'Location: '.DBA_URL.'expenses/'.$action.'.html' ) ;
	}
