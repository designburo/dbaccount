<?php
if(!defined("MAIN")) die("No direct access");
$menu[5]['active'] = 'uk-active';
$smarty->assign('title', DBA_NAME . " -- "._CONTACTS);


//echo "action = ".$action;
switch ($action) {
	case "new" :
		$smarty->assign( 'action', "new");
		$smarty->assign( 'additional_info', _CONTACT_INFO_NEW);
		$customers = $dba->db_get("*", "customer","type","customer");
		if($customers['error_status']==true) {
			if( $customers['error_info']!=="noresult" ) {
				$smarty->assign( 'do_we_have_customers', false );
			}
		} else {
			$smarty->assign( 'do_we_have_customers', true );
			unset($customers['error_status']);
			$smarty->assign( 'customers', $customers );
		}
		$smarty->assign( 'screen', 'contacts-new_edit' );
		break;
	case "edit" :
		$id=$_SESSION['id'];
		$contact = $dba->db_get("*", "contacts", "id", $id);
		$smarty->assign( 'action', "edit");
		$smarty->assign( 'additional_info', "");
		$smarty->assign( 'contact', $contact[0] );
		$smarty->assign( 'screen', 'contacts-new_edit' );
		$customers = $dba->db_get("*", "customer","type","customer");
		if($customers['error_status']==true) {
			if( $customers['error_info']!=="noresult" ) {
				$smarty->assign( 'do_we_have_customers', false );
			}
		} else {
			$smarty->assign( 'do_we_have_customers', true );
			unset($customers['error_status']);
			$smarty->assign( 'customers', $customers );
		}
		break;

	case "view" :
		$id=$_SESSION['id'];
		$contact = $dba->db_get("*", "contacts", "id", $id, false);
		if($contact==false) {
			$msg.=_CONTACT_UNKOWN."<BR>";
			$_SESSION['msg']=$msg;
			$_SESSION['msgtype']="alert";
			session_write_close();
			header( 'Location: '.DBA_URL.'contacts.html' ) ;
		}
		$customer = $dba->db_get("*", "customer", "id", $contact[0]['customer_id'],false);
		if($customer==false) {
			$msg.=_CONTACT_CUSTOMER_UNKOWN."<BR>";
			$_SESSION['msg']=$msg;
			$_SESSION['msgtype']="alert";
			session_write_close();
			header( 'Location: '.DBA_URL.'contacts.html' ) ;
		}
		$orders = $dba->db_get("*", "orders", "contacts_id", $id);
		if($orders['error_status']==true) {
			$order=false;
		}
		$smarty->assign( 'screen', 'contacts-view' );
		$smarty->assign( 'contact', $contact[0] );
		$smarty->assign( 'customer', $customer[0] );
		$smarty->assign( 'orders', $orders );
		$smarty->assign( 'additional_info', "");

		//print_r($customer[0]);
		break;
	case "" :
		$contacts=$dba->db_get( "*", "contacts" );
		if($contacts['error_status']==true) {
			if( $contacts['error_info']=="noresult" ) {
					// No customers yet
				$smarty->assign( 'screen', 'contacts-new_edit' );
				$smarty->assign( 'additional_info', _CONTACT_INFO_NORESULTS);
				$smarty->assign( 'action', "none");
				$smarty->assign( 'title', DBA_NAME . ' -- '._CONTACTS );
				$action_menu=array();
				$action_menu[0]['link']=DBA_URL."contacts/new.html";
				$action_menu[0]['title']=_CONTACT_ACTION_NEW;
				$action_menu[0]['icon']="plus";
				$smarty->assign ( 'action_menu', $action_menu);
				}	else {
					$msg = _DBA_ERROR." : ".$contacts['error_info'];
				}
			} else {
				// we have results so list
				$action_menu=array();
				$action_menu[0]['link']=DBA_URL."contacts/new.html";
				$action_menu[0]['title']=_CONTACT_ACTION_NEW;
				$action_menu[0]['icon']="plus";
				unset($contacts['error_status']);
				foreach ($contacts as $key=>$contact) {
					$company = $dba->db_get("name","customer","id",$contact['customer_id'],false);
					$contacts[$key]['company']=$company[0]['name'];
				}
				$smarty->assign ( 'action_menu', $action_menu);
				$smarty->assign( 'action', "list");
				$smarty->assign( 'additional_info', "");
				$smarty->assign( 'screen', 'contacts-list' );
				$smarty->assign( 'contacts', $contacts );
			}
			break;
	}
