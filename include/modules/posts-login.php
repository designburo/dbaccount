<?php
error_reporting( E_ALL );
if(isset($_POST['username']) && ($_POST['username']!== "")) {
	$user['username']=$dba->db_real_escape($_POST['username']);
} else {
	$error .= "no username<BR>";
}

if(isset($_POST['password']) && ($_POST['password']!== "")) {
	$user['password']=$dba->db_real_escape($_POST['password']);
} else {
	$error .= "no password<BR>";
}

if($error!=="")
{
	$msg.=$error."<BR>"._POST_ERRORS."<BR>";
	$_SESSION['msg']=$msg;
	$_SESSION['msgtype']="warning";

} else {
	if($dba->checkUser($user)) {
		$_SESSION['user']=$user['username'];
		$_SESSION['pass']=$user['password'];
		$msg.="Welcome back <strong>".$user['username']."</strong>!<br>";
		$_SESSION['msg']=$msg;
		$_SESSION['msgtype']="success";
	} else {
		$msg.="Incorrect username or password";
		$_SESSION['msg']=$msg;
		$_SESSION['msgtype']="danger";
	}

}
session_write_close();
header( 'Location: '.DBA_URL.'home.html' ) ;

