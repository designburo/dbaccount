<?php
if(isset($_POST['action']) and $_POST['action']!=="")
{
	$action=$_POST['action'];
}
 else {
	 $msg.="no action defined";
 }

//echo $action;
//exit;
if($action=="new" || $action=="edit-save")
{

	if(!$contact['title']=$dba->checkPost($_POST,"title")) {
		$contact['title']="";
	}

	if(!$contact['fname']=$dba->checkPost($_POST,"fname")) {
		$error .= _POST_EMPTY." "._GENERAL_FNAME."<BR>";
	}

	if(!$contact['lname']=$dba->checkPost($_POST,"lname")) {
		$error .= _POST_EMPTY." "._GENERAL_LNAME."<BR>";
	}

	if(!$contact['initials']=$dba->checkPost($_POST,"initials")) {
		$contact['initials']=substr($contact['fname'],0,1).".".substr($contact['lname'],0,1).".";
	}

	if(!$contact['phone']=$dba->checkPost($_POST,"phone")) {
		$contact['phone']="";
	}

	if(!$contact['customer_id']=$dba->checkPost($_POST,"customer")) {
		$error .= _POST_EMPTY." "._CONTACT_COMPANY."<BR>";
	}

	if(!$contact['email']=$dba->checkPost($_POST,"email")) {
		$contact['email']="";
	}

	if(!$contact['skype']=$dba->checkPost($_POST,"skype")) {
		$contact['skype']="";
	}

	if(!$contact['note']=$dba->checkPost($_POST,"note")) {
		$contact['note']="";
	}


}

if($action=="new") {

	if($error!=="")
	{
		$_SESSION['action']='new';
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();

	} else {
		$msg.=_POST_CONTACT_NEW_POST_OK."<BR>";
		$_SESSION['msg']=$msg;

		session_write_close();
		$cid = $dba->newContact($contact);
		if(file_exists($_FILES["photo"]["tmp_name"]) || is_uploaded_file($_FILES['photo']['tmp_name'])) {
			if($_FILES['photo']['size']>2120000) {
				$error .= _POST_EMPTY." "._POST_CONTACT_PHOTO_TOBIG."<BR>";
			} else {
				$imageData = @getimagesize($_FILES["photo"]["tmp_name"]);
				if($imageData === FALSE || !($imageData[2] == IMAGETYPE_GIF || $imageData[2] == IMAGETYPE_JPEG || $imageData[2] == IMAGETYPE_PNG)) {
					$error .= _POST_EMPTY." "._POST_CONTACT_PHOTO_WRONG."<BR>";
				} else {
					$ext = pathinfo($_FILES["photo"]["name"], PATHINFO_EXTENSION);
					$f = DBA_PATH."/uploads/contacts/".$cid.".".$ext;
					move_uploaded_file($_FILES["photo"]["tmp_name"], $f);
					$contact['logo'] = DBA_URL."uploads/contacts/".$cid.".".$ext;
					$contact['id']=$cid;
					$dba->updateContact($contact);
				}
			}
		}
		//print_r($comp);
		//exit;
		header( 'Location: '.DBA_URL.'contacts.html' ) ;
	}

}

if($action=="notes")
{
	if(isset($_POST['edit_note'])) {

	}
	if(isset($_POST['delete_note'])) {
		$dba->deleteNote($_POST['delete_note']);
		$msg.=_NOTE_DELETE_OK."<BR>";
		$_SESSION['msg']=$msg;
	}
	$_SESSION['id']=$_POST['id']; // so we jump back to the right customer

	session_write_close();
	header( 'Location: '.$_POST['note_return'] );

}

if($action=="edit-save") {

	if($error!=="")
	{

		$_SESSION['action']='edit';
		$_SESSION['id']=$_POST['id'];
		$msg.=$error."<BR>"._POST_ERRORS."<BR>";
		session_write_close();
		$action="edit";
		header( 'Location: '.DBA_URL.'contacts/'.$action.'.html' ) ;

	} else {
		$msg.=_POST_CONTACT_EDIT_POST_OK."<BR>";
		$_SESSION['msg']=$msg;
		$contact['id']=$_POST['id'];
		session_write_close();
		if(file_exists($_FILES["photo"]["tmp_name"]) || is_uploaded_file($_FILES['photo']['tmp_name'])) {
			if($_FILES['photo']['size']>2120000) {
				$error .= _POST_EMPTY." "._POST_CONTACT_PHOTO_TOBIG."<BR>";
			} else {
				$imageData = @getimagesize($_FILES["photo"]["tmp_name"]);
				if($imageData === FALSE || !($imageData[2] == IMAGETYPE_GIF || $imageData[2] == IMAGETYPE_JPEG || $imageData[2] == IMAGETYPE_PNG)) {
					$error .= _POST_EMPTY." "._POST_CONTACT_PHOTO_WRONG."<BR>";
				} else {
					$ext = pathinfo($_FILES["photo"]["name"], PATHINFO_EXTENSION);
					$f = DBA_PATH."/uploads/contacts/".$contact['id'].".".$ext;
					move_uploaded_file($_FILES["photo"]["tmp_name"], $f);
					$contact['photo'] = DBA_URL."uploads/contacts/".$contact['id'].".".$ext;
				}
			}
		}
		$dba->updateContact($contact);
		//print_r($comp);
		//exit;
		header( 'Location: '.DBA_URL.'contacts.html' ) ;
	}
}



if($action=="detail") {

	if(isset($_POST['view']) && ($_POST['view']!=="")) {
		$action="view";
		$id=$_POST['view'];
		$sub="view";
	}
	if(isset($_POST['edit']) && ($_POST['edit']!=="")) {
		$action="edit";
		$id=$_POST['edit'];
		$sub="edit";
	}

	$_SESSION['id']=$id;
	header( 'Location: '.DBA_URL.'contacts/'.$action.'.html' ) ;
	}

