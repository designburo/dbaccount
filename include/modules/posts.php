<?php
$module = $_POST['module'];
$error = "";

switch($module) {
	case "api" :
		include (DBA_MODULES."api.php");
		exit;
	case "settings" :
		include (DBA_MODULES."posts-preferences.php");
		break;
	case "vats" :
		include (DBA_MODULES."posts-vats.php");
		break;
	case "company" :
		include (DBA_MODULES."posts-company.php");
		break;
	case "customers" :
		include (DBA_MODULES."posts-customer.php");
		break;
	case "contacts" :
		include (DBA_MODULES."posts-contact.php");
		break;
	case "email" :
		include (DBA_MODULES."posts-mail.php");
		break;
	case "login" :
		include (DBA_MODULES."posts-login.php");
		break;
	case "items" :
		include (DBA_MODULES."posts-items.php");
		break;
	case "orders" :
		include (DBA_MODULES."posts-orders.php");
		break;
	case "quotes" :
		include (DBA_MODULES."posts-quotes.php");
		break;
	case "expenses" :
		include (DBA_MODULES."posts-expenses.php");
		break;
	case "payments" :
		include (DBA_MODULES."posts-payments.php");
		break;
	case "translate" :
		include (DBA_MODULES."posts-translate.php");
		break;
}
