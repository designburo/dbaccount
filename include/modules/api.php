<?php
$error="";

if(isset($_POST['action']) && ($_POST['action']!== ""))
{
	$action=$_POST['action'];
} else {
	$error .= "no action";
}

if($action=="taxes") {
	if(isset($_POST['year']) && $_POST['year']!=="") {
		// code for showing tax overview
		$year = $_POST['year'];
		$taxes['orders']['first_quarter']=$dba->get_totals_between("orders", $year."-01-01", $year."-03-31");
		$taxes['orders']['second_quarter']=$dba->get_totals_between("orders", $year."-04-01", $year."-06-30");
		$taxes['orders']['third_quarter']=$dba->get_totals_between("orders", $year."-07-01", $year."-09-30");
		$taxes['orders']['fourth_quarter']=$dba->get_totals_between("orders", $year."-10-01", $year."-12-31");
		$taxes['orders']['this_year']=$dba->get_totals_between("orders", $year."-01-01", $year."-12-31");
		$taxes['orders']['total']=$dba->get_totals_between("orders", false,"");
		$taxes['expenses']['first_quarter']=$dba->get_totals_between("expenses", $year."-01-01", $year."-03-31");
		$taxes['expenses']['second_quarter']=$dba->get_totals_between("expenses", $year."-04-01", $year."-06-30");
		$taxes['expenses']['third_quarter']=$dba->get_totals_between("expenses", $year."-07-01", $year."-09-30");
		$taxes['expenses']['fourth_quarter']=$dba->get_totals_between("expenses", $year."-10-01", $year."-12-31");
		$taxes['expenses']['this_year']=$dba->get_totals_between("expenses", $year."-01-01", $year."-12-31");
		$taxes['expenses']['total']=$dba->get_totals_between("expenses", false,"");

		$taxes['totals']['first_quarter']['amount_in']=number_format((float)$taxes['orders']['first_quarter']['total_in']-(float)$taxes['expenses']['first_quarter']['total_in'],2);
		$taxes['totals']['first_quarter']['amount_ex']=number_format((float)$taxes['orders']['first_quarter']['total_ex']-(float)$taxes['expenses']['first_quarter']['total_ex'],2);
		$taxes['totals']['first_quarter']['amount_vat']=number_format((float)$taxes['orders']['first_quarter']['total_vat']-(float)$taxes['expenses']['first_quarter']['total_vat'],2);

		$taxes['totals']['second_quarter']['amount_in']=number_format((float)$taxes['orders']['second_quarter']['total_in']-(float)$taxes['expenses']['second_quarter']['total_in'],2);
		$taxes['totals']['second_quarter']['amount_ex']=number_format((float)$taxes['orders']['second_quarter']['total_ex']-(float)$taxes['expenses']['second_quarter']['total_ex'],2);
		$taxes['totals']['second_quarter']['amount_vat']=number_format((float)$taxes['orders']['second_quarter']['total_vat']-(float)$taxes['expenses']['second_quarter']['total_vat'],2);

		$taxes['totals']['third_quarter']['amount_in']=number_format((float)$taxes['orders']['third_quarter']['total_in']-(float)$taxes['expenses']['third_quarter']['total_in'],2);
		$taxes['totals']['third_quarter']['amount_ex']=number_format((float)$taxes['orders']['third_quarter']['total_ex']-(float)$taxes['expenses']['third_quarter']['total_ex'],2);
		$taxes['totals']['third_quarter']['amount_vat']=number_format((float)$taxes['orders']['third_quarter']['total_vat']-(float)$taxes['expenses']['third_quarter']['total_vat'],2);

		$taxes['totals']['fourth_quarter']['amount_in']=number_format((float)$taxes['orders']['fourth_quarter']['total_in']-(float)$taxes['expenses']['fourth_quarter']['total_in'],2);
		$taxes['totals']['fourth_quarter']['amount_ex']=number_format((float)$taxes['orders']['fourth_quarter']['total_ex']-(float)$taxes['expenses']['fourth_quarter']['total_ex'],2);
		$taxes['totals']['fourth_quarter']['amount_vat']=number_format((float)$taxes['orders']['fourth_quarter']['total_vat']-(float)$taxes['expenses']['fourth_quarter']['total_vat'],2);

		$smarty->assign("taxes", $taxes);
		$smarty->assign( 'prefs', $prefs);
		$smarty->assign('DBA_URL', DBA_URL);
		$smarty->assign('DBA_PATH', DBA_PATH);
		$smarty->assign('theme', $theme);
		$smarty->left_delimiter = '-{-';
		$smarty->right_delimiter = '-}-';
		$error = $smarty->fetch('api_taxes.html');
	} else {
		$error = "Specify year";
	}
}

if($action=="shownotes") {
	if(isset($_POST['id']) && ($_POST['id']!== "")) {
		$id=$_POST['id'];
	} else {
		$error .= "no id";
		break 2;
	}
	$notes = $dba->db_get_notes("customer_id", $id, true);
	if($notes['error_status']==true) {
			if( $notes['error_info']=="noresult" ) {
			echo "No notes yet";
			exit;
		}
	}
	unset($notes['error_status']);
	$smarty->assign("notes", $notes);
	$smarty->assign("id", $_POST['id']);
	$smarty->assign('DBA_URL', DBA_URL);
	$smarty->assign('DBA_PATH', DBA_PATH);
	$smarty->assign('theme', $theme);
	$smarty->left_delimiter = '-{-';
	$smarty->right_delimiter = '-}-';
	$error = $smarty->fetch('api_notes.html');
}


if($action=="showcontact") {
	if(isset($_POST['id']) && ($_POST['id']!== "")) {
			$id=$_POST['id'];

		if(isset($_POST['cid']) && ($_POST['cid']!== "")) {
			$cid=$_POST['cid'];
		} else {
			$cid=false;
		}
		$contact=$dba->db_get("*","contacts","customer_id", $id);

		if($contact['error_status']==true) {
				if( $contact['error_info']!=="noresult" ) {
				$error .= _DBA_ERROR." : ".$contact['error_info'];
			}
		} else {

				unset($contact['error_status']);
				$ret='
				<label class="uk-form-label" for="order_contact">'._ORDERS_CONTACTS_NAME.'</label>
				<div class="uk-form-controls">
					<select id="order_contact" name="order_contact" class="uk-width-1-1">
					<option disabled selected value="0"> -- '._ORDERS_SELECT_CONTACT.' -- </option>';
					foreach ($contact as $one)
					{
						$ret.='<option value="'.$one['id'].'" ';
						if ($cid) {
							$ret .= 'selected';
						}
						$ret.= '>'.$one['fname'].' '.$one['lname'].'</option>'."\n";
					}
					$ret.='
								</select>
						</div>';
					$error=$ret;
		}
	} else {
		$error .= "no id";
	}
}

if($action=="showcontacts") {
	if(isset($_POST['id']) && ($_POST['id']!== "")) {
		$id=$_POST['id'];
	} else {
		echo "no id";
		exit;
	}
	$contacts = $dba->db_get("*","contacts","customer_id", $id);
	if($contacts['error_status']==true) {
			if( $contacts['error_info']=="noresult" ) {
				echo "No contacts yet";
				exit;
		}
	}
	unset($contacts['error_status']);
	$smarty->assign("contacts", $contacts);
	$smarty->assign("id", $_POST['id']);
	$smarty->assign('DBA_URL', DBA_URL);
	$smarty->assign('DBA_PATH', DBA_PATH);
	$smarty->assign('theme', $theme);
	$smarty->left_delimiter = '-{-';
	$smarty->right_delimiter = '-}-';
	$error = $smarty->fetch('api_contacts.html');
}

if($action=="showorders") {
	if(isset($_POST['id']) && ($_POST['id']!== "")) {
		$id=$_POST['id'];
	} else {
		echo "no id";
		exit;
	}
	$type="order";
	//$what, $id, $type, $cid=false
	$orders = $dba->db_getOrders("*",$id,$type);

	if($orders['error_status']==true) {
			if( $orders['error_info']=="noresult" ) {
				echo "No orders yet";
				exit;
		}
	}
	unset($orders['error_status']);
	$orders=$dba->addOrderNr($orders);
	$orders=$dba->addOrderDetail($orders);
	$smarty->assign( 'prefs', $prefs);
	$smarty->assign("orders", $orders);
	$smarty->assign("id", $_POST['id']);
	$smarty->assign('DBA_URL', DBA_URL);
	$smarty->assign('DBA_PATH', DBA_PATH);
	$smarty->assign('theme', $theme);
	$smarty->left_delimiter = '-{-';
	$smarty->right_delimiter = '-}-';
	$error = $smarty->fetch('api_orders.html');
}

echo $error;
exit;
