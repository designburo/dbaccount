<?php
if(!defined("MAIN")) die("No direct access");
$menu[1]['active'] = 'uk-active';

$smarty->assign( 'title', DBA_NAME . " -- "._INVOICES);
$action_menu=array();
$action_menu[0]['link']=DBA_URL."orders/new.html";
$action_menu[0]['title']=_ITEMS_ACTION_NEW_TITLE;
$action_menu[0]['icon']="plus";
$smarty->assign ( 'action_menu', $action_menu);
$smarty->assign ( 'currency', $prefs['currency']);
	$cid=false;
if($sub=="customer") {
	$cid=$action;
	$action="new";
}

switch ($action) {
	case "edit" :
		$id=$_SESSION['id'];
		$get_order = $dba->db_get("*", "orders", "id", $id, false);
		$details = $dba->db_get("*", "order_details", "order_id", $id, false);
		$order=$get_order[0];
		$order['date']=date("Y-m-d",strtotime($order['date']));
		$order['description']=base64_decode($order['description']);
		$order['description']=str_replace('\r\n','',$order['description']);
		$customers = $dba->db_get("*", "customer", "type", "customer");
		if($customers['error_status']==true) {
			$msg = _DBA_ERROR." : ".$customers['error_info'];
			} else {
				unset($customers['error_status']);
				$smarty->assign( 'customers', $customers);
				$smarty->assign( 'additional_info', _ORDERS_INFO_NEW);
				$items = $dba->getItemsVats();
				if($items['error_status']==true) {
					if($items['error_info']=="noresult") {
						$smarty->assign( 'noitems', true);
					} else {
					$msg = _DBA_ERROR." : ".$customers['error_info'];
					}
				} else {
					unset($items['error_status']);
					$smarty->assign( 'noitems', false);
					$smarty->assign( 'items', $items);
					$smarty->assign( 'order', $order);
					$smarty->assign( 'screen', 'orders-edit' );
					$smarty->assign( 'details', $details );
			}
		}

		break;
	case "new" :
		$smarty->assign( 'screen', 'orders-new' );
		$customers = $dba->db_get("*", "customer","type","customer");
		if($customers['error_status']==true) {
			if($customers['error_info']=="noresult") {
				$nfo = _ORDERS_NO_CUSTOMERS.'<a href="'.DBA_URL.'customers/new.html" class="uk-button uk-button-primary">'._ORDERS_NO_CUSTOMERS_LINK."</a>";
				$smarty->assign( 'additional_info', $nfo);
				$smarty->assign( 'nocustomers', true);
			}	else {
				$msg = _DBA_ERROR." : ".$customers['error_info'];
			}
		} else {
			unset($customers['error_status']);
			if ($cid) {
				$smarty->assign( 'customer_id', $cid);
			} else {
				$smarty->assign( 'customer_id', false);
			}
			$smarty->assign( 'nocustomers', false);
			$smarty->assign( 'customers', $customers);
			$smarty->assign( 'additional_info', _ORDERS_INFO_NEW);
			$items = $dba->getItemsVats();
			if($items['error_status']==true) {
				if($items['error_info']=="noresult") {
					$smarty->assign( 'noitems', true);
				} else {
				$msg = _DBA_ERROR." : ".$customers['error_info'];
				}
			} else {
				unset($items['error_status']);
				$smarty->assign( 'noitems', false);
				$smarty->assign( 'items', $items);



			}
		}
		break;
	case "pdf" :
		$_SESSION['type']="pdf";
		echo "<script>
			var strWindowFeatures = 'menubar=yes,location=no,resizable=yes,scrollbars=yes,status=yes,modal=yes';
			var win = window.open('../../print.html','Print', strWindowFeatures);

		</script>
		";

		$smarty->assign( 'additional_info', _ORDERS_PRINT);
		if(isset($_SESSION['return'])) {
			if($_SESSION['return']=="view"){
				goto view;
			}

		}
		goto cont;

	case "view" :
		view:
		include $theme."print/invoices.php";
		$smarty->assign( 'print_lang', $print_lang );
		$id=$_SESSION['id'];
		$order=$dba->db_get("*", "orders", "id" ,$id,false);
		$order=$order[0];
		$order['number']=$dba->orderNumber($order['id'],$order['date']);
		$order['date']=date("d-m-Y",strtotime($order['date']));
		if ($order['description']!="") {
			$order['description']=base64_decode($order['description']);
			$order['description']=str_replace('\r\n','',$order['description']);
		} else {
			$order['description']=false;
		}

		$customer=$dba->db_get("*", "customer", "id" ,$order['customer_id'],false);
		$customer=$customer[0];
		if($order['contacts_id']!="0") {
			$contact=$dba->db_get("fname, lname, title", "contacts", "id" ,$order['contacts_id'],false);
			$contact=$contact[0];
			$customer['contact']=$contact['title']." ".$contact['fname']." ".$contact['lname'];
		} else {
			$customer['contact']=false;
		}
		if($customer['address_2']!="") {
			$customer['address']=$customer['address_1']."<br>".$customer['address_2'];
		} else {
			$customer['address']=$customer['address_1'];
		}

		$company=$dba->db_get("*", "company", "id" ,1,false);
		$company=$company[0];
		$company['address']=$company['address_1'];
		if ($company['address_2']!=="") {
			$company['address'].="<br>".$company['address_1'];
		}
		$payments=$dba->db_get("*", "payments", "order_id", $order['id']);
		if($payments['error_status']==true) {
			$order['haspayments']=false;
		} else {
			$order['haspayments']=true;
			unset($payments['error_info']);
			unset($payments['error_status']);
		}
		$total_payments=0;
		if($order['haspayments']) {
			 $total_payments=0;
				foreach ($payments as $key=>$payment) {
					$payments[$key]['date']=date("d-m-Y", strtotime($payment['date']));
					$total_payments+=$payment['amount'];
				}
		}
		$total_payments=number_format($total_payments,2);
		$order_open=number_format($order['amount_in']-$total_payments,2);


		$order['hasitems']=false;
		$order['hasexpenses']=false;
		$expenses=$dba->db_get("*", "expenses", "order_id", $order['id']);
		//print_r($expenses);
		if($expenses['error_status']==true) {
			$order['hasexpenses']=false;
		} else {
			$order['hasexpenses']=true;
			unset($expenses['error_info']);
			unset($expenses['error_status']);
		}
		if($order['hasexpenses']) { // we have expenses
			//echo "ok";
			$expense['total_amount_ex']=0;
			$expense['total_vat']=0;
			$expense['total_amount_in']=0;
			foreach ($expenses as $key=>$value) {
				$expense['total_amount_ex']+=$value['amount_ex'];
				$expense['total_amount_in']+=$value['amount_in'];
				$expense['total_vat']+=$value['vat'];
				$expenses[$key]['date']=date("d-m-Y", strtotime($value['date']));
			}
			$expense['total_amount_ex']=number_format($expense['total_amount_ex'],2);
			$expense['total_amount_in']=number_format($expense['total_amount_in'],2);
			$expense['total_vat']=number_format($expense['total_vat'],2);
			$order['total_vat']=number_format($order['vat']-$expense['total_vat'],2);
			$order['total_amount_in']=number_format($order['amount_in']-$expense['total_amount_in'],2);
			$order['total_amount_ex']=number_format($order['amount_ex']-$expense['total_amount_ex'],2);
			if ($order['total_amount_ex'] < 0) {
				$order['warning']="red";
			} else {
				$order['warning']="";
			}
		} else {
			$expense=false;
		}

		$qrcode="Invoice for\n".$customer['name']."\n";
		$qrcode.=$customer['address']."\n";
		$qrcode.=$customer['pcode']." ".$customer['city']."\n\n";
		$qrcode.="Invoice #".$order['number']."\n";
		$qrcode.="Invoice date : ".$order['date']."\n";
		$qrcode.="Invoice due before : ".date('d-m-Y',strtotime("+".$prefs['paydays']." day", strtotime($order['date'])))."\n\n";
		$qrcode.="Total price incl. VAT : EUR ".$order['amount_in']."\n";
		$qrcode.="Payment can be made through PayPal (creditcard and iDeal) to ".$company['email']."\n";
		$qrcode.="or\n";
		$qrcode.="By bank to ".$company['name']." at bankaccount #".$company['bank']." -- Always mention the invoice number";
		$order['qrcode']=$dba->qrcode($qrcode,400);
		$items=$dba->db_get("*", "order_details", "order_id" ,$order['id'],false);
		if($items!==false){
			$order['hasitems']=true;
			foreach ($items as $key=>$item) {
				$items[$key]['vat_type']=$dba->getVatName($item['vat_id']);
			}
		}
		$smarty->assign( 'company', $company );
		$smarty->assign( 'order', $order );
		$smarty->assign( 'order_open', $order_open );
		$smarty->assign( 'total_payments', $total_payments );
		$smarty->assign( 'payments', $payments );
		$smarty->assign( 'customer', $customer );
		$smarty->assign( 'expenses', $expenses );
		$smarty->assign( 'expense', $expense );
		$smarty->assign( 'additional_info', "");
		$smarty->assign( 'items', $items );
		$smarty->assign( 'prefs', $prefs );
		$smarty->assign( 'screen', 'orders-view' );
		break;

	case "print" :
		$_SESSION['type']="print";
		echo "<script>
			var strWindowFeatures = 'menubar=yes,location=no,resizable=yes,scrollbars=yes,status=yes,modal=yes';
			var win = window.open('../../print.html','Print', strWindowFeatures);

		</script>
		";

		$smarty->assign( 'additional_info', _ORDERS_PRINT);
		if(isset($_SESSION['return'])) {
			if($_SESSION['return']=="view"){
				goto view;
			}

		}

cont:
	case "" :

	$orders = $dba->db_get("id, amount_in, date, status, customer_id", "orders" ,"type", "order");
	if($orders['error_status']==true) {
		if( $orders['error_info']=="noresult") {
			// No taxes yet
			$smarty->assign( 'additional_info', _ORDERS_INFO_NORESULTS);
			$smarty->assign( 'screen', 'orders-empty' );

		}	else {
			$msg = _DBA_ERROR." : ".$orders['error_info'];
		}
	} else {
		// we have results
		unset($orders['error_status']);
		$orders=$dba->addOrderNr($orders);
		$orders=$dba->addOrderDetail($orders);
		//print_r($orders);
		$smarty->assign( 'prefs', $prefs);
		$smarty->assign( 'additional_info', "");
		$smarty->assign( 'action', 'list');
		$smarty->assign( 'screen', 'orders-list' );
		$smarty->assign( 'orders', $orders );
		unset($orders['error_status']);

	}
		break;
}
