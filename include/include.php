<?php

	if(!defined("MAIN")) die("No direct access");

	function getSettings()
	{
		$ret = json_decode(file_get_contents(DBA_PATH."/include/settings.json"), true);
		return $ret;
	}
	function saveSettings($prefs)
	{
		file_put_contents(DBA_PATH."/include/settings.json", json_encode($prefs));
	}
	function createSettings()
	{
		$prefs=array();
		$prefs['name']="DB Accounts";
		$prefs['theme']="default";
		$prefs['lang']="english";
		$prefs['currency']="euro";
		$prefs['paydays']=7;
		$prefs['order_number']=1;
		$prefs['timezone']="Europe/Amsterdam";
		$prefs['colorscheme']=".almost-flat";
		saveSettings($prefs);
		return $prefs;
	}

	function breadCrumb($page,$sub,$action) {
		$ret=array();
		$ret[0]['name']=DBA_NAME;
		$ret[0]['link']=DBA_URL;
		if($page==strtolower(_HOME)) {
			return $ret;
		}
		$ret[1]['name']=$page;
		$ret[1]['link']=DBA_URL.$page.".html";
		if($sub) {
			$ret[2]['name']=$sub;
			$ret[2]['link']=DBA_URL.$page."/".$sub.".html";
		}
		if($action!=="" && $sub) {
			$ret[3]['name']=$action;
			$ret[3]['link']=DBA_URL.$page."/".$sub."/".$action.".html";
		}
		if($action!=="" && !$sub) {
			$ret[2]['name']=$action;
			$ret[2]['link']=DBA_URL.$page."/".$action.".html";
		}
		return $ret;

	}
	function defineDefault()
	{

	}


	/*
	echo "<p>dba_path:".DBA_PATH."</p>";
	echo "<p>dba_modules:".DBA_MODULES."</p>";
	echo "<p>dba_xtra_path:".DBA_XTRA_PATH."</p>";
	echo "<p>dba_url:".DBA_URL."</p>";
	echo "<p>dba_uploads:".DBA_UPLOADS."</p>";
	*/
	//Load Settings
	if(file_exists(DBA_PATH."/include/settings.json"))
	{
		$prefs=getSettings();
	}
	else
	{
		$prefs=createSettings();
	}

	date_default_timezone_set($prefs['timezone']);
	define ( 'DBA_NAME', $prefs['name']);
	//echo DBA_PATH."/themes/".$prefs['theme']."/theme.html";
	if(file_exists(DBA_PATH."/themes/".$prefs['theme']."/theme.html"))
	{
		$theme=DBA_PATH."/themes/".$prefs['theme']."/";
	}
	else
	{
		$theme=DBA_PATH."/themes/default/";
	}
	$lpath=DBA_PATH . "/languages/".$prefs['lang']."/lang.php";
	if(!file_exists($lpath))
	{
		$prefs['lang']="english";
	}
	include (DBA_PATH . "/languages/".$prefs['lang']."/lang.php");
	include ("dba_class.php");

?>
