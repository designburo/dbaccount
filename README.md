![logo-small.png](https://bitbucket.org/repo/7kxeRM/images/2169040752-logo-small.png "This is our awesome logo")

PHP development on a program for self employed people or small businesses to create invoices and estimates, expenses, keep track of VAT and get quarterly or yearly balance results. Should be theme based for the program and the reports, as well as the invoices/estimates. Multiple languages.
There currently is nothing out there with the same features as DBaccount, be it for good or for worse :) SimpleInvoices does have similar features.

Most parts have been build. Including a build-in tool for translating to other languages.
~~No installer present yet, although a correct .sql file for importing is present.~~
An installer script is added and will start automatically.
Some settings (like database table prefix and default username and password and more) ~~still need to be set within the dba Class.~~ Are now set by the installer script.

If you feel like contributing, send me a message and I'll setup a quick installation guide.

Program is done by 90%. Still not use-able in a production environment. Current language maintained and build upon is English.

For an overview of details and installation instructions. [Visit the Wiki](https://bitbucket.org/designburo/dbaccount/wiki/Home)