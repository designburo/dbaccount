<?php
session_start();
error_reporting( E_ALL );
define( 'MAIN',true );
$_SESSION['path']=substr(substr(__FILE__, strlen(realpath($_SERVER['DOCUMENT_ROOT']))), 0, - strlen(basename(__FILE__)));
if(file_exists("include/config.php")) {
	include_once("include/config.php");
	if(!defined("DBA_URL")) {
		header( 'Location: installer/install.php' ) ;
	}
} else {
	header( 'Location: installer/install.php' ) ;
}
define ( 'DBA_PATH', dirname(__FILE__) );
define ( 'DBA_MODULES', DBA_PATH."/include/modules/");
define ( 'DBA_XTRA_PATH', DBA_PATH."/dba_xtra");
define ( 'DBA_UPLOADS', DBA_PATH."/uploads/");
include( 'include/include.php' );

require( DBA_PATH.'/include/smarty/Smarty.class.php' );
if(isset($_SESSION['action']) && $_SESSION['action']!=="")
{
	$action = $_SESSION['action'];
	unset($_SESSION['action']);
} else {
	$action="";
}
//echo $action;
/* Smarty elements */
$smarty = new Smarty();
$smarty->setTemplateDir( $theme );
$smarty->setCompileDir( DBA_XTRA_PATH . '/templates_c' );
$smarty->setCacheDir( DBA_XTRA_PATH . '/cache' );
$smarty->setConfigDir( DBA_XTRA_PATH . '/configs' );
$smarty->caching = Smarty::CACHING_OFF;

if(isset($_SESSION['user']) && $_SESSION['user']!=="")
{
	$user = $_SESSION['user'];
} else {
	$user=false;
}

if(isset($_SESSION['pass']) && $_SESSION['pass']!=="")
{
	$pass = $_SESSION['pass'];
} else {
	$pass=false;
}

$dba=new dba();
if($user!==false && $pass!==false)
{
	$chk['username']=$user;
	$chk['password']=$pass;
	if (!$dba->checkUser($chk)) {
		$user=false;
		$pass=false;
	}
}

/* Assign Smarty variables */

$menu = array();
$menu[0]['name']=_HOME;
$menu[0]['link']=DBA_URL."home.html";
$menu[0]['active']="";
$submenu[_HOME]=false;
$menu[1]['name']=_ORDERS;
$menu[1]['link']=DBA_URL."orders.html";
$menu[1]['active']="";
$submenu[_ORDERS]=array();
$submenu[_ORDERS][0]['name']=_ORDERS;
$submenu[_ORDERS][0]['link']=DBA_URL."orders.html";
$submenu[_ORDERS][1]['name']=_PAYMENTS;
$submenu[_ORDERS][1]['link']=DBA_URL."payments.html";
$menu[2]['name']=_ESTIMATES;
$menu[2]['link']=DBA_URL."quotes.html";
$menu[2]['active']="";
$submenu[_ESTIMATES]=false;
$menu[3]['name']=_EXPENSES;
$menu[3]['link']=DBA_URL."expenses.html";
$menu[3]['active']="";
$submenu[_EXPENSES]=false;
$menu[4]['name']=_CUSTOMERS;
$menu[4]['link']=DBA_URL."customers.html";
$menu[4]['active']="";
$submenu[_CUSTOMERS]=false;
$menu[5]['name']=_CONTACTS;
$menu[5]['link']=DBA_URL."contacts.html";
$menu[5]['active']="";
$submenu[_CONTACTS]=false;
$menu[6]['name']=_TAXES;
$menu[6]['link']=DBA_URL."taxes.html";
$menu[6]['active']="";
$submenu[_TAXES]=false;
$menu[7]['name']=_PREFERENCES;
$menu[7]['link']=DBA_URL.strtolower(_PREFERENCES).".html";
$menu[7]['active']="";
$submenu[_PREFERENCES]=array();
$submenu[_PREFERENCES][0]['name']=_PREFS_COMPANY;
$submenu[_PREFERENCES][0]['link']=DBA_URL."preferences/company.html";
$submenu[_PREFERENCES][1]['name']=_PREFS_SETTINGS;
$submenu[_PREFERENCES][1]['link']=DBA_URL."preferences/settings.html";
$submenu[_PREFERENCES][2]['name']=_VAT;
$submenu[_PREFERENCES][2]['link']=DBA_URL."preferences/vats.html";
$submenu[_PREFERENCES][3]['name']=_ITEMS;
$submenu[_PREFERENCES][3]['link']=DBA_URL."preferences/items.html";
$submenu[_PREFERENCES][5]['name']=_TRANSLATE;
$submenu[_PREFERENCES][5]['link']=DBA_URL."translate.html";
$submenu[_PREFERENCES][6]['name']=_LOGOUT;
$submenu[_PREFERENCES][6]['link']=DBA_URL."preferences/logout.html";

$smarty->assign('dba_content', "");
$msg="";  // Notification content
$content=""; // Main content
if(isset($_POST['module']) && $_POST['module']!=="") {
	include (DBA_MODULES."posts.php");
}
if(!$user) {
	$smarty->assign('DBA_URL', DBA_URL);
	$smarty->assign('DBA_PATH', DBA_PATH);
	$smarty->assign('theme', $theme);
	$smarty->assign('name', $prefs['name']);
	$smarty->assign('dba_footer', '');
	$smarty->left_delimiter = '-{-';
	$smarty->right_delimiter = '-}-';
	$smarty->assign('title', DBA_NAME . " -- Login");
	if(isset($_SESSION['msg'])) {
		$msg=$_SESSION['msg'];
		unset($_SESSION['msg']);
		if(isset($_SESSION['msgtype'])) {
			$msgtype=$_SESSION['msgtype'];
			unset($_SESSION['msgtype']);
		}
		$smarty->assign('notification', $dba->createNotification($msgtype,$msg));
	} else {
		$smarty->assign('notification', "");
	}
	$smarty->display('login.html');
	exit;
}
if(!isset($_GET['page']) || $_GET['page']=="") /*default page*/
{
	$smarty->assign('title', DBA_NAME . " -- Home");
	$page="home";
}
else
{
	$page=$_GET['page'];
}
if(!isset($_GET['sub']) || $_GET['sub']=="") /*sub page*/
{
	$sub=false;
}
else
{
	$sub=$_GET['sub'];
}


if($action=="") {
	if(!isset($_GET['action']) || $_GET['action']=="") /*sub page*/
	{
		$action="";
	}
	else
	{
		$action=$_GET['action'];
	}
}

$smarty->assign('breadcrumb', breadCrumb($page, $sub, $action));

if (!$dba->checkInstallation())
		{
			//$content='<p>'._DB_NA.'<a href="'.DBA_URL.'install.html">'._DB_INSTALL.'</a></p>';
			$msg.=_DB_NA.'<a href="'.DBA_URL.'install.html">'._DB_INSTALL.'</a><BR>';
			error_reporting(E_ALL);
		}

$msgtype="";
//load the menu
include (DBA_MODULES."menu.php");


if($msg!=="" || isset($_SESSION['msg']))
{
	$tst="";
	if(isset($_SESSION['msg'])) {
		$tst=$_SESSION['msg'];
		unset($_SESSION['msg']);
	}
	if($msg!=="") {
		$msg=substr($msg, 0, -4);
	} else {
		$tst=substr($tst, 0, -4);
	}
	$tst.=$msg;
	if(isset($_SESSION['msgtype'])) {
		$msgtype=$_SESSION['msgtype'];
		unset($_SESSION['msgtype']);
	}
	$smarty->assign('notification', $dba->createNotification($msgtype,$tst));
}
else
{
	$smarty->assign('notification', "");
}

$smarty->assign('menu', $menu);
$smarty->assign('user', $user);
$smarty->assign('submenu', $submenu);
$smarty->assign('DBA_URL', DBA_URL);
$smarty->assign('DBA_PATH', DBA_PATH);
$smarty->assign('theme', $theme);
$smarty->assign('dba_content', $content);
$smarty->assign('name', $prefs['name']);
$smarty->assign('dba_footer', '');
$smarty->assign('smtp', $dba->smtp['status']);
$smarty->assign('logo', $dba->getLogo());
$smarty->assign('colorscheme', $prefs['colorscheme']);
$smarty->left_delimiter = '-{-';
$smarty->right_delimiter = '-}-';
//$smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
//$smarty->setCompileCheck(false);
unset($_SESSION['pdf']);
if(isset($_SESSION['pdf'])) {
	$out=$smarty->fetch('print.html');
	require_once('include/tcpdf/tcpdf.php');

	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 061');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 061', PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 10);

	// add a page
	$pdf->AddPage();

	/* NOTE:
	 * *********************************************************
	 * You can load external XHTML using :
	 *
	 * $html = file_get_contents('/path/to/your/file.html');
	 *
	 * External CSS files will be automatically loaded.
	 * Sometimes you need to fix the path of the external CSS.
	 * *********************************************************
	 */

	// define some HTML content with style
	$html = $out;

	// output the HTML content
	$pdf->writeHTML($html, true, false, true, false, '');

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	// reset pointer to the last page
	$pdf->lastPage();

	// ---------------------------------------------------------

	//Close and output PDF document
	$pdf->Output('example_061.pdf', 'I');
	}
		 else {
		$out=$smarty->display('theme.html');
	}


?>
