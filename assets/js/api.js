
$(document).ready(function(){
	$('#api-notes').click(function(){ // Load the notes
		$("#api-load-notes").load( "-{-$DBA_URL-}-api.html", { module : "api", action : "shownotes", id : "-{-$customer.id-}-"});
	});
	$('#api-contacts').click(function(){ // Load the contacts
		$("#api-load-contacts").load( "-{-$DBA_URL-}-api.html", { module : "api", action : "showcontacts", id : "-{-$customer.id-}-"});
	});
});
