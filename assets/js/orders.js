	$(document).ready(function()
		{
			var regex = /^(.+?)(\d+)$/i;
			var cloneIndex = $('#itemLines tr').length-2;
			//alert (cloneIndex);
			var addrow = $("#addrow");
			function clone(){
				var row = $('#0').clone(true)
				.insertBefore(addrow)
				.attr("id", cloneIndex)
				.find("*")
				.each(function() {
					var id = this.id || "";
					var match = id.match(regex) || [];
					if (match.length == 3) {
						this.id = match[1] + (cloneIndex);
					}
				})
				$('#amount' + cloneIndex).prop('required', true);
				$('#description' + cloneIndex).prop('required', true);
				$('#vat' + cloneIndex).prop('required', true);
				$('#vatid' + cloneIndex).prop('required', true);
				$('#price' + cloneIndex).prop('required', true);
				$('#total' + cloneIndex).prop('required', true);
				cloneIndex++;
			}
			function remove(){
				if ((cloneIndex-1)>0) {
					$("#" + (cloneIndex-1)).remove();
					cloneIndex--;
				}
			}
			$("#add_row").click(function()
			{
				clone();
				doTheMath();
			});
			$("#delete_row").click(function()
			{
			 remove();
				doTheMath();
			 });

			function doTheMath() {
				var total = 0;
				for (i =1; i<=cloneIndex; i++)
					{
						var amount= parseFloat($("#amount" + i).val());
						var price= parseFloat($("#price" + i).val());
						//alert(amount + "*" + price);
						if (!isNaN(amount) && !isNaN(price))
							{
								$("#total" + i).val( parseFloat(amount*price).toFixed(2)) ;
							}
					}
				added = 0;
				$("input[id^='total']").each(function()
				{
				var num=parseFloat(this.value,10);
				if(!isNaN(num)) {
					added += num;
				}
				});
				$("#order_total_ex").val(parseFloat(added).toFixed(2));
				//calculate percentages
				var vattotal=0;
				for (i =0; i<=cloneIndex; i++)
					{
						var vat=parseFloat($("#vat" + i).val());
						var totalitemprice=parseFloat($("#total" + i).val());
						if (!isNaN(vat) && !isNaN(totalitemprice) || (vat==0 || totalitemprice==0))
						{
							vattotal += (totalitemprice / 100) * vat;
						}
					}
				$("#order_total_vat").val(parseFloat(vattotal).toFixed(2));
				var exbtw = parseFloat($("#order_total_ex").val());
				$("#order_total_in").val( parseFloat(vattotal + exbtw).toFixed(2));
			}

			$("input[id^='amount']").on("change", function(){doTheMath()});

			$("select").change(function()
				{
					var sid = $(this).closest( 'tr' ).attr('id');
					var selected = $(this).find(':selected');
					//alert ("closest tr : "+sid);
					$("#description"+sid).val(selected.data('name'));
					$("#price"+sid).val(selected.data('price'));
					$("#vat"+sid).val(selected.data('vat'));
					$("#vatid"+sid).val(selected.data('vatid'));
				doTheMath();
				});

			$("input[id^='total']").on("change paste keyup", function()
			{
				doTheMath();
			});

			$("input[id^='price']").on("change paste keyup propertychange", function()
			{
				doTheMath();
				});

			$("input[id^='vat']").on("change paste keyup propertychange", function()
			{
				doTheMath();
				});




		});
