-- DBaccunt SQL Dump
--
-- Generation Time: Jan 28, 2017 at 11:01 AM
-- Server version: 5.1.36
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


-- --------------------------------------------------------

--
-- Table structure for table `dba_company`
--

CREATE TABLE IF NOT EXISTS `dba_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address_1` varchar(45) DEFAULT NULL,
  `address_2` varchar(45) DEFAULT NULL,
  `pcode` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `email` varchar(70) NOT NULL,
  `website` varchar(150) NOT NULL,
  `vat` varchar(20) NOT NULL,
  `coc` varchar(30) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `contact` varchar(60) NOT NULL,
  `skype` varchar(50) NOT NULL,
  `bank` varchar(50) NOT NULL,
  `swift` varchar(50) NOT NULL,
  `logo` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dba_contacts`
--

CREATE TABLE IF NOT EXISTS `dba_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(45) NOT NULL,
  `lname` varchar(45) NOT NULL,
  `initials` varchar(45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `email` varchar(70) NOT NULL,
  `skype` varchar(100) NOT NULL,
  `note` varchar(5000) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dba_customer`
--

CREATE TABLE IF NOT EXISTS `dba_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address_1` varchar(45) DEFAULT NULL,
  `address_2` varchar(45) DEFAULT NULL,
  `pcode` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `website` varchar(60) DEFAULT NULL,
  `note` mediumtext,
  `type` varchar(50) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dba_expenses`
--

CREATE TABLE IF NOT EXISTS `dba_expenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `date` datetime NOT NULL,
  `amount_in` decimal(8,2) NOT NULL,
  `amount_ex` decimal(8,2) NOT NULL,
  `type` varchar(45) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `invoice` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `vat` decimal(8,2) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dba_files`
--

CREATE TABLE IF NOT EXISTS `dba_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `file` varchar(500) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `expense_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dba_items`
--

CREATE TABLE IF NOT EXISTS `dba_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `vat_id` int(11) NOT NULL,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dba_notes`
--

CREATE TABLE IF NOT EXISTS `dba_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `note` text NOT NULL,
  `type` varchar(30) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dba_orders`
--

CREATE TABLE IF NOT EXISTS `dba_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount_ex` decimal(8,2) NOT NULL,
  `amount_in` decimal(8,2) NOT NULL,
  `date` datetime NOT NULL,
  `description` varchar(5000) NOT NULL,
  `description_internal` varchar(5000) NOT NULL,
  `vat` decimal(8,2) NOT NULL,
  `note` mediumtext NOT NULL,
  `status` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `contacts_id` int(11) NOT NULL,
  `expences_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dba_order_details`
--

CREATE TABLE IF NOT EXISTS `dba_order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(60) NOT NULL,
  `amount` float NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `price_total` decimal(8,2) NOT NULL,
  `vat` decimal(8,2) NOT NULL,
  `vat_id` int(11) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dba_payments`
--

CREATE TABLE IF NOT EXISTS `dba_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `type` varchar(45) NOT NULL,
  `note` varchar(3000) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dba_settings`
--

CREATE TABLE IF NOT EXISTS `dba_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `smtp_host` varchar(60) NOT NULL,
  `smtp_port` mediumint(9) NOT NULL,
  `smtp_user` varchar(60) NOT NULL,
  `smtp_pass` varchar(60) NOT NULL,
  `smtp_auth` tinyint(1) NOT NULL,
  `note` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dba_vat`
--

CREATE TABLE IF NOT EXISTS `dba_vat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `vat` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
